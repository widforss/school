# Föreläsning 2016-09-12

## Returvärden

    # coding=utf-8

    import math

    def circleArea(radius):
        temp = math.pi * radius ** 2
        return temp

    def cylinderVolume(radius, length):
        return circleArea(radius)*length

    def main():
        print "-- Volymen av en cylinder --"
        r = input("Radie: ")
        l = input("Längd: ")
        v = cylinderVolume(r, l)
        print "Volym: ", v

    main()

    ''' output:
	-- Volymen av en cylinder --
	Radie: 1
	Längd: 2
	Volym:  6.28318530718
	'''

När man anropar en funktion som anropar ett värde antar funktionsanropet det returnerade värdet, precis som i `v = cylinderVolume(r, l)`.



## Rekursion

Fakultetsfunktionen är per definition rekursiv, därför är den bra för att demonstrera hur rekursion fungerar.

	def factorial(n):
		if n == 0:
			return 1
		else:
			recurse = factorial(n - 1)
			result = n * recurse
			return result

	print factorial(6)

	''' output:
	720
	'''

<span></span>

    Beräkna factorial(3):

    n == 3, så vi kör else-satsen.
	Vi ropar på factorial(3 - 1)

		n == 2, så vi kör else-satsen.
		Vi ropar på factorial(2-1)

			n == 1, så vi kör else-satsen.
			Vi ropar på factorial(1 - 1)

				n == 0, så vi returnerar 1

			result = n * 1 == 1 * 1
			result är 1, det är värdet vi returnerar

		result = n * 1 == 2 * 1
		result är 2 , det är värdet vi returnerar

    result = n * 2 == 3 * 2
    result är 6, det är värdet vi returnerar

Samma program kan skrivas som:

	def fact(n):
		if n == 0:
			return 1
		else:
			return n * fact(n - 1)

	print "fact(3)= ", fact(3)

	''' output:
    fact(3)=  6
    '''

### Basfall och rekursionsfall

I ett basfall har man ingen rekursion. Genom att ha ett basfall får man möjlighet att avbryta rekursionen så att programmet kan termineras. Det är lättast om man skriver detta sist så att man ser åt vilket håll rekursionen arbetar. Oftast är basfallet 0.

		if n == 0:
			return 1

Rekursionsfallet är den kod som utför den faktiska rekursionen. Denna måste hela tiden arbeta sig fram mot basfallet.

		else:
			return n * fact(n - 1)

## Loopar

*Iteration* är beteckningen på fenomenet som loopar används till.

Variabler kan tilldelas ett värde och direkt därefter tilldelas ett annat värde.

tecknet `=` betyder inte *lika med* utan är ett tecken för tilldelning. I `bruce = 5` tilldelas variabeln `bruce` värdet 5, men det är inte lika med 5. För att tydliggöra detta kan man se nedanstående kod.

    stefan = 5
    bruce = stefan
    stefan = 8
    print stefan
    print bruce

    ''' output:
    8
    5
    '''

`bruce` tilldelas samma värde som `stefan` har, därefter ändras `stefan`s värde, men `bruce` behåller samma värde som `bruce` *tilldelades* tidigare. 

### while

Upprepar en satslista tills uttrycket är falskt.

    while uttryck:
        satslista

<span></span>

    def countdown(n):
        while n > 0:
            print n
            n = n - 1
        print "Blastoff!"

    countdown(10)

    ''' output:
    10
    9
    8
    7
    6
    5
    4
    3
    2
    1
    Blastoff!
    '''

### Loopar kontra rekursion

* En loop kan alltid översättas till rekursion, men inte alltid tvärtom
    * Rekursion är alltså kraftfullare än snurror i denna mening
* Enkla problem kan ofta uttryckas väldigt enkelt med snurror
* Svårare problem tenderar att uttryckas felaktigt med hjälp av snurror
* Loopar återanvänder minne varje varv, medan rekursion medför en ständigt växande stack
* Rekursion drar lite mer minne, men det är inget att vara rädd för, det är bara att blåsa på


### Fibonacci

    def fibonacci(n):
        if n == 0 or n == 1:
            return 1
        else:
            return fibonacci(n - 1) + fibonacci(n -2)

    print fibonacci(37)

    ''' output:
    39088169
    '''

    Ovanstående tvingas göra samma beräkning många gånger, och problemet växer exponentiellt med storleken på `n`. Det gör att programmet snabbt blir väldigt långsamt. Det liknar en forkbomb, gör inte så här. Det har att göra med att vi har två funktionsanrop.
