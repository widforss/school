# Föreläsning 2016-09-19

Dagens föreläsning handlar om strängar.

## Strängar

En sträng är

* ett värde som kan lagras som en variabel, eller
* en struktur för att lagra ett antal värden. Då kallas beståndsdelarna tecken och är ... tecken.

Vill man ha ett enskilt tecken kan man använda indexering.

    fruit = "orange"
    letter = fruit[1]
    print letter

    ''' output:
    r
    '''

Notera att första tecknet i en sträng är indexerat till `0`.

För att ta reda på hur lång en sträng är kan man använda funktionen `len()`.

    fruit = "orange"
    print len(fruit)

    ''' output:
    6
    '''

Detta kan man använda för att hitta sista tecknet i en sträng.

    fruit = "orange"
    print fruit[len(fruit) - 1]
    
    ''' output:
    e
    '''

Alternativt kan man bara skriva

    fruit = "orange"
    print fruit[-1]

    ''' output:
    e
    '''

Vill man ha tag på en del av en sträng kan man använda `:`.

    fruit = "orange"
    print fruit[0:2]
    print fruit[:2]
    print fruit[3:]

    ''' output:
    or
    or
    nge
    '''

Notera att det sista elementet inte tas med, `fruit[0:2]` väljer element `1` och `2`.

## Strängtraversering

Vill man genomföra en operation på varje element kan man använda en `while`- eller `for`-loop. Här följer en `while`-loop.

    fruit = "orange"
    index = 0
    while index < len(fruit):
        letter = fruit[index]
        print letter
        index = index + 1

    ''' output:
    o
    r
    a
    n
    g
    e
    '''

Samma sak kan uttryckas mer koncist med en `for`-loop.

    fruit = "orange"
    for letter in fruit:
        print letter

    ''' output:
    o
    r
    a
    n
    g
    e
    '''

## Strängoperatorer

Givetvis kan man jämföra likheter mellan strängar.

    print "banan" == "banana"

    ''' output:
    False
    '''

Även olikheter fungerar.

    print "banan" != "banana"

    ''' output:
    True
    '''

Större än och mindre än testar om en sträng är före eller efter en annan om de ligger i bokstavsordning enligt ASCI-tabellen.

    print "banan" > "banana"
    print "banan" > "%"

    ''' output:
    False
    True
    '''

![ASCI-tabellen](asci.png)

Vill man veta vad ett tecken har för position i tabellen eller vad en position har för tecken kan man använda `ord()` och `chr()`.

## Listor

En sträng är en sekvens av tecken. En lista är en sekvens av vad som helst. Notera att de är *olika typer och inte kompatibla*.

    [10, 20, 30, 40]
    ["spam", "bungee", "swallow"]
    []
    [[1,2], [], [], [2,2,3]]

Listor indexeras på exakt samma sätt som en sträng. En skillnad är att om man indexerar ett intervall i en lista får man en lista, medan om man indexerar ett element får man bara det elementet.

    print type("abcde"[2])
    print type("abcde"[2:3])
    print type(['a', 'b', 'c', 'd', 'e'][2])
    print type(['a', 'b', 'c', 'd', 'e'][2:3])

    ''' output:
    <type 'str'>
    <type 'str'>
    <type 'list'>
    <type 'str'>
    '''

## Mutation


