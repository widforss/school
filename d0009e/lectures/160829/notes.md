# Föreläsning 2016-08-29

Presentation

* [administrativ info](./adminfo.pdf)
* [Programmets väg](./lecture01.pdf)
* [Variabler, uttryck och satser](./lecture02.pdf)

## Kursinfo

Labbgrupper:

* Grupp A
	* Datateknik Civ. (Efternamn t.o.m. "Ström")
* Grupp B
	* Datateknik Civ. (Efternamn fr.o.m. "Sund")
	* Datateknik Högskole.
	* Industriell ekonomi
* Grupp C
	* Övriga

Kursen består av föreläsningar i helsal samt labbar.

[Fredrik Bengtsson](mailto:bson@ltu.se) är examinator.

Lämna in labben _innan_ deadline. Särskilt labb tre och fyra.

[Kurshemsida](http://www.sm.luth.se/csee/courses/d0009e)

Schema:
* [Grupp A](https://se.timeedit.net/web/ltu/db1/schedule1/riqv6QY75ZXZ1qQy367Q4Q006gZ726Z3Q0Y0351Q9Yo6o.html)
* [Grupp B](https://se.timeedit.net/web/ltu/db1/schedule1/riqv6QY75ZXZ1qQy367Q5Q006gZ726Z3Q0Y0351Q9Yo7o.html)
* [Grupp C](https://se.timeedit.net/web/ltu/db1/schedule1/riqv6QY75ZXZ1qQy367Q5Q006gZ726Z3Q0Y0351Q9Yo7o.html)

Labb: 3hp
Tenta 28/10: 4,5 hp
anmäl till tentan senast 7/10

Vi har 2 föreläsningar i veckan och 3 labbar i veckan.

Labbarna är i A3017 och A3019.

* Labbar utförs individuellt.
* Önskvärt 
    * muntliga diskussioner om labbarna
* Förbjudet
	* Kopiering av filer
	* simultant skrivande av lösning för samma program.
		
Litteraturen: [How to think like a computer scientist](../../lit-thinkpython/thinkCSpy.pdf)

Det finns en kursutvärdering i samband av tentan. Tidigare utvärderingar har gjort att det nu finns frivilliga svårare uppgifter vid labbarna. Dessa ger inga poäng.

## Vad är programmering?

Programmering är ett sätt att tänka, programspråken är små och enkla. Det svåra är att lära sig hur man tänker.

* En bra tysk författare kommer skriva en bra svensk bok om denne lär sig svenska. En svensk som inte kan skriva en bok kommer inte att skriva någon bra bok trots att den är bra på svenska.

## Varför Python?

Kursen tar främst upp Python 2.7, men teorin är applicerbart på de flesta språk. Python 2 och Python 3 är inte direkt kompatibla med varandra. Python är ett nytt språk (90-talet). Äldre språk, t.ex. C, är från 60-talet. Python är lätt att lära och används mycket i industrin. Det har stora standardbibliotek. Det hjälper en att inte uppfinna hjulet. Java har också stort standardbibliotek.

## Vad är en dator?

En dator är en maskin utan uppgift. Poängen med en dator är att du berättar för den vad den ska göra. Det gör man genom att programmera. Det handar om att lagra siffror i serie. Datorn tolkar därefter serierna som ett program och utför det man (förhoppningsvis) ville göra.

## Programmeringsparadigmer

Imperativ programmering är det som brukar kallas programmering. Det finns dessutom deklarativa språk med underfamiljerna funktionella och logikspråk. Dessa är utom kursens ramar. Det går att kombinera två språk i ett program, men det blir snabbt krångligt.

Ett program är en sekvens av instruktioner. Dessa är skrivna på ett programmeringsspråk. Varje instruktion ändrar någonting.

## Ett program skrivet på svenska

* häll i vatten i behållaren
	* innehåller många steg som vi gömmer undan (ett bibliotek)
* ta fram ett kaffefilter
	* Ta fram kaffefiltret _innan_ du häller i kaffet (rätt ordning)
* häll i en skopa kaffe i filtret
* upprepa ovanstående tills tillräckligt med kaffe finns i filtret
	* en loop. Det är viktigt att något kan förändras så att loopen inte fastnar
* montera filtret
* slå på bryggaren
* titta efter om kaffet är klart
* upprepa ovanstående tills kaffet är klart
	* Så länge man inte gör ett realtidssystem måste man kolla hela tiden, det går inte att få bryggaren att säga *pling*
* häll upp kaffet

## Vad är ett program?

Datorn förstår inte våra programmeringsspråk. Datorn förstår istället maskinkod. Man översätter alla språk till maskinkod innan det exekveras. Detta sker på två olika sätt.

Översättningen sker genom interpretering eller exekvering. Interpreterande språk kör en tolk som översätter vårt språk i realtid. Exekvering innebär att man översätter programmet i förhand, det kallas kompilering. Det kompilerade programmet körs effektivare men själva kompileringen tar tid. Det får inte vara fel på kompilatorn.

Kursen kommer att använda utvecklingsmiljön *IDLE*. Det är uppbyggt så att man har en editor som man skriver programmet i. När man interpreterar skriptet körs det i ett separat fönster.

## Två exempelprogram

    print 1+1

    ''' output:
    2
	'''
<span></span>

    print "Hello World!"

    ''' output:
    Hello World!
	'''
## Felsökning

*Prompten* går att använda direkt, det är bara att skriva direkt. Det är bra för att felsöka och förstå. Det är en fördel med interpreterade språk, kompilerade språk har ingen prompt.

Det är viktigt att testa sina program eftersom alla program innehåller fel. Alla gör fel. Skillnaden är att erfarna programmerare hittar felen. Det enda som blir bättre med tiden är att man lär sig att inte strukturera sitt program fel.

## Byggstenarna i ett program

* Hämta indata (tangentbord, fil, nätverk)
* Producera utdata (på skärm, fil, nätverk)
* Matematiska operationer (+, -, *)
* Villkorlig körning (testa och ta beslut)
* Repetition

Programmering handlar om att sätta samman byggstenar till meningsfulla större enheter. Förvänta dig att andra kommer att läsa dina program, så skriv de snyggt.

## Dokumentera!

Det är möjligt att skriva ett program som man själv inte förstår det inom några minuter.

> You know you are a genius, but you may want to understand yourself two weeks from now.

## Typer och variabler

Notera att citattecknen i print "Hello World!" försvinner vid interpreteringen. Citattecken innesluter strängar. Strängar är godtyckliga sekvenser av tecken. Dessa är till för programmet och är inte en del av språket.

I ett programspråk kan man definiera variabler. Dessa är efter definitionen lika med det man definierat det till.

    v = "Hello World!"
	print v

	''' output:
	Hello World!
	'''

Variabler har olika typer. En variabel med värdet 2 har typen integer, 2.0 float, "Hello" sträng. Värt att notera är att float _inte_ är lika med decimaltal, då det finns oändligt många decimaltal men ett begränsat antal float.
