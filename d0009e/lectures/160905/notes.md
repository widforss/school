# Föreläsning 2016-09-05

## Kommentarer

Kommentarer påverkar inte programflödet. Det är endast till för att kommentera själva källkoden. Det är svårt att kommentera för mycket, så känn inte att du skriver för mycket.

I Python börjar kommentarer med #. Allt efter # är en kommentar innan nästa radbrytning.

    # Här är en kommentar
	print "test" # Här är en annan kommentar

Kommentarer kan också användas för att kommentera ut kod

    #addition = 1 + 1

## Programflöde

När interpreteraren ser en funktionsdeklaration körs den inte. Däremot är funktionen deklarerad efter att interpreteraren har sett den. Det betyder att den kan användas.

    def printolle():
	    print "olle"
	
	print "maria"
	printolle()

	''' output:
	maria
	olle
	'''

Funktioner ska användas för att man ska slippa skriva samma rad kod två gånger.

Ett skript läses alltid uppifrån och ner. Dock kan en funktion köras genom att den anropas. Det kan liknas med att det görs ett tillfälligt hopp i koden till funktionen.

## Parametrar

Funktioner har ofta parametrar. Det är variabler som funktioner behöver för att utföra sin funktion. I funktionsdeklarationen anges vad de parametrarna heter. När funktionen anropas anges vilka värden som parametrarna ska ha. Därefter används parametrarna som lokala variabler. Dessa är helt isolerade från variablerna i övriga programmet och kan ha samma namn som en helt annan variabel i övriga programmet.

    def addThreePrint(a,b,c):
		r = a+b+c
		print a
		print r
		a=0
		b=0
		c=0
		r=0
		print "lokala a,b,c,r:",a,b,c,r
	
	a=1
	b=2
	c=3
	addThreePrint(b,a,c)
	print a,b,c
	addThreePrint(6,0,1)
	#print r

	''' output:
	2
	6
	lokala a,b,c,r: 0 0 0 0
	1 2 3
	6
	7
	lokala a,b,c,r: 0 0 0 0
	'''

Som synes finns det två variabler vardera av namnet a, b och c. Dessa har inget med varandra att göra utan finns i olika sammanhang. Det är viktigt att förstå att det bara är funktionsargumentens värde och inte deras namn som används som parametrar.

De variabler som uppstår i en funktion försvinner när den slutar. Det är viktigt när man gör rekursiva funktioner.

Parametrar är exakt samma sak som lokala variabler med skillnaden att de tilldelas ett värde från start.

## Stack

Man kan se ett program som ett uppochnedvänt träd där huvudfunktionen är roten. Varje funktion är en gren och varje gren kan dela sig igen. Alla variabler är lokala till just sin gren.

## Felmeddelanden

Python har relativt fina felmeddelanden som berättar vad som är fel. Man får veta precis vilka funktioner som anropas för att det ska bli fel och det står precis vad som är fel.

    def printTwice( bruce ):
		print bruce, bruuce

	def catTwice( part1, part2 ):
		cat = part1 + part2
		printTwice( cat )

	chant1 = "Pie Jesu domine, "
	chant2 = "Dona eis requiem."
	catTwice( chant1, chant2 )

	''' output:
	Traceback (most recent call last):
		File "<pyshell#38>", line 1, in -toplevel-
			catTwice(chant1, chant2)
		File "<pyshell#31>", line 3, in catTwice
			printTwice( cat )
		File "<pyshell#37>", line 2, in printTwice
			print bruce, bruuce
	NameError: global name 'bruuce' is not defined
	>>> 
	'''

![Stack-diagram (slide)](stack_diagram.png)

## Funktioner med resultat

`int()`, `float()` och `string()` är exempel på funktioner som returnerar ett resultat. Det innebär att man inte ändrar något direkt med funktionen utan

## Boolska uttryck

Det finns en typ som heter `bool`. Den har två möjliga värden, `True` eller `False`. Det är dessa man använder vid `if`-satser och så vidare. Man använder booleska operatorer med dessa värden. Exempel på dessa är `or`, `and`, `not` osv osv.

### Villkorssats (if-sats)

    if x > 0:
		print "x is positive"
	elif x < 0:
		print "x is negative
	else
		print "x equals 0"

Det är inte mer komplicerat än så.

    def test(a)
	    b = a > 12
		if b:
			print "a > 12"
		if a > 12 and a < 20:
			print "a är också mindre än 20^
		if a > 12 and a < 20 and a == 16:
			print "a = 16"

Det finns i Python ett specialfall där man kan skriva `if 0 < x < 10:`. Det kan man normalt inte använda i andra språk utan då måste man skriva `0 < x and x < 10`.

## Return

`return` avbryter en funktion omedelbart. Programmet fortsätter då direkt därifrån funktionen anropades ifrån.
