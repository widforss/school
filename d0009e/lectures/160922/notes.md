# Föreläsning 2016-09-22

Idag handlar föreläsningen om *tupler* och *dictionaries*.

## Sammansatta datatyper

Python har flera *sammansatta datatyper*.

* Strängar
    * Sekvenser av tecken
    * Icke muterbara
* Listor
    * Sekvenser av vad som helst
* Tupler
    * Sekvenser av vad som helst
    * Icke muterbara
* Dictionaries
    * "Listor" där index kan vara vad som helst

## Tupler

Tupler fungerar nästan precis som listor med undantaget att det är icke muterbara. Syntaxmässigt skrivs de:

    (1, 2, "3")
    (1,)
    (,)

Eftersom man inte kan mutera tupler är de bra att använda till vissa saker, till exempel som nycklar i *dictionaries*. I de flesta språk är listor en typ som består av element av samma typ men som har variabel längd. Tupler är då en typ med fix längd men med element som kan bestå av många olika typer. I Python är den enda skillnaden att tupler är icke muterbara, det vill säga att man inte kan ändra på elementen i tupeln. Dock kan det vara en bra idé att göra som ovan även i Python.

Tupler kan vara bra för att returnera värden från funktioner.

	def bounds(list):
		min = max = list[0]
		for x in list:
			if x < min:
				min = x
			if x > max:
				max = x
		return (min, max)

## Dictionaries

Typerna strängar, listor och tupler indexeras med heltal. Dictionaries är en sammansatt typ med ett index som kan bestå av vad som helst. Både index och element kan alltså bestå av olika typer.

Ett exempel på en dictionary:
	
	{'one': 'uno', 'two': 'dos', 'three': 'tres'}

För att få ett specifikt värde gör man precis som med övriga sammansatta datatyper.

	eng2sp = {'one': 'uno', 'two': 'dos', 'three': 'tres'}
	print eng2sp['one']

	''' output:
	uno
	'''

Använder man dictionaries får man inte bry sig om värdenas ordning. Det är heller inte trivialt att köra en dictionary baklänges.

Precis som med listor kan man ta reda på en dictionarys längd.

	len(eng2sp)

Man kan radera element.

	del eng2sp['uno']

Vill man veta vad som finns i listan kan man få en lista på nycklar. Man kan också ta reda på om en nyckel finns.

	eng2sp.keys()          # Ger en lista med nycklar.

	eng2sp.has_key('one')  # Returnerar ett boolskt värde.
	'one' in eng2sp.keys() # Samma som ovan

## Alias

Sammansatta typer beter sig annorlunda vid tilldelning.

    a = 74
	b = a
	a = 73
	print b

	''' output:
	74
	'''

<span></span>

	a = [1, 2, 3]
	b = a
	a[0] = 8
	print b[0]

	''' output:
	8
	'''

Det här kallas alias, man har flera *representanter* för samma lista. Vill man ha en kopia av en lista kan man använda ett kolon.

	a = [1, 2, 3]
	b = a[:]
	a[0] = 8
	print b[0]

	''' output:
	1
	'''

Det här används vid funktionsanrop för att ändra direkt i variabler utan att behöva kopiera hela värdet.

	def deleteHead(list)
		list[0:1] = []
	
	numbers = [1, 2, 3]
	deleteHead(numbers)
	print numbers

	''' output:
	[2, 3]
	'''
