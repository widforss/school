# Föreläsning 2016-08-31

Det går att tänka på många sätt när man programmerar. Se till att tänka på samma sätt som språket du skriver i, annars blir det jobbigt.

## Värden och typer

De saker en dator manipulerar kallas värden. Dessa värden har olika typer. Ett heltal är, precis som det låter, ett heltal. Det finns också flyttal, som motsvaras av decimaltal. Det spelar egentligen ingen roll idag om man använder flyttal istället för heltal, men de olika tygerna har av hävd olika användningsområden.

Strängar är en annan typ. Det är en sekvens av tecken. Det är inte en siffra, även om de enskilda tecknen kan vara en siffra. Jämför "3.14".

Flyttal skrivs med decimalpunkt, i Python betecknar ett komma en lista.

    print 3.14

	''' output:
	3.14
	'''
<span></span>
    print 3,14

	'''output:
	3 14
	'''
	
Typer:

* Heltal
	* Numeriska värden utan decimaler
	* 4
* Flyttal
	* numeriska värden med decimaler
	* 7.9
* Strängar
    * textvärden
	* "banan"

## Variabler, uttryck och satser

Variabler är ett sätt att ge namn åt värden. Även strängar är värden. Allt inom dubbla eller enkla citattecken är strängar.

En sats gör en sak. print är ett exempel på en sats. Nedan ser man också hur man deklarar variabler.

    s = "Hello World!"
	s2 = "print"
    print "s"
	print s2

	''' output:
	s
	print

Uttryck:

* 1+1

Sats:

* print

För att lära sig Python kan man använda prompten. Där kan man skriva programmen en sats i taget medan man kör det. Det används dock inte i praktiken.

Variabler är ett sätt att lagra data i datorns minnet. Man kan skriva direkt till datorns minne, men det är ingen bra idé.

	print "-- Mitt program --"
	v = 3
	g = 4.4
	print v,"multilicerat med ",g," blir ",v*g
	print "-- Slut på mitt program --"

	''' output:
	-- Mitt program --
	3 multilicerat med  4  blir  12
	-- Slut på mitt program --
    '''

Inom matematiken ändrar man aldrig en variabel när man definierat den, men inom programmering ändrar man ofta på redan deklarerade variabler. En variabel är inte lika med sitt ursprungliga värde, den är bara tilldelad samma värde.

Variabler får inte heta vad som helst. Varje språk har ett antal reserverade ord, till exempel *if*, *and* och *from*. Dessa får inte användas som variabelnamn. Man får inte heller börja ett variabelnamn med en siffra. Normalt får man inte heller ha specialtecken i variabelnamn. Normalt unviker man att få problem med reglerna genom att använda vettiga namn.

Kom ihåg att aldrig enbart skriva ett uttryck utan sats i ett skript.

## Operatorer och operander

De flesta operatorer är precis som i vanlig text, t.ex. +, -, /. Dock är vissa operatorer (t.ex. potenser) svåra att skriva i vanlig text. Därför skiljer dessa operatorer mellan språk. I Python markeras potenser med \*\*.

I Python 2 är en division med två heltal en heltalsdivision.

    print 10/3

    ''' output:
    3
    '''

Python följer samma precedens som matematiken.

Vissa typer går inte att kombinera.

    message = "abc"

    message-1
    message*"Hello"
    "15"+2

Däremot kan man använda vissa operatorer som liknar sina matematiska motsvarigheter med strängar.

    print "Fun"*3

    ''' output:
    FunFunFun
    '''
<span></span>
    print "abc"+"def"

    ''' output:
    abcdef
    '''

## Funktioner

Om man skriver log(x) så får man logaritmen för x. Det är ett exempel på en funktion där log är namnet på funktionen och x är argumentet. log() är inte inbyggt i Python utan finns i en separat modul *math*. En modul är samma sak som ett bibliotek. Man påbörjar sina skript genom att importera de moduler man behöver.

    import math

    print math.log(100)

Standardbiblioteken hjälper oss att slippa uppfinna hjulet flera gånger. Samma sak är det med egenskrivna funktioner. Genom att gömma undan kod bakom en funktion slipper man skriva samma kod flera gånger.

När man namnger funktioner ska man använda ett namn som beskriver vad funktionen gör. Det gör inget om funktionsnamnet är lite långt.

I Python skriver man en funktion såhär:

	def funktionsnamn(parameterlista):
		satslista

Om man vill ha en funktion som skriver en tom rad kan man skriva såhär.

	def newLine():
		print

    ''' När man har definierat en funktion kan man anropa den.
    ''''

    newLine()

    ''' Därefter kan man anropa den hur många gånger som helst. Funktionen körs en gång varje gång den anropas.
    '''

    newLine()
    newLine()
    newLine()
    newLine()

Varför ska man ha funktioner?

* De ökar läsbarheten genom att gömma undan kod
* Det blir kortare program eftersom man bara skriver samma kod en gång.A

Programflödet körs normalt uppifrån och ner. Funktionsanrop gör att man tillfälligt hoppar till en annan plats i skriptet. Efter att funktionsanropet har interpreterats hoppar man dock tillbaka till den tidigare positionen.

En funktion ska ha en effekt *eller* ett värde. Antingen skriver man ut "*3*" eller så returnerar man "*3*". Aldrig båda. Vill man ha båda anropar man två funktioner.
