# Föreläsning 2016-09-08

Dagens föreläsning handlar om rekursion.

## Rekursion

I Python kan man anropa alla funktioner från vilken funktion som helst. Det innemär att man an anropa en funktion från sig själv.

    def countdown(n):
	    if n == 0:
		    print "Blastoff!"
	    else:
		    print n,
			countdown(n - 1)
	
	countdown(8)

	''' output:
	8 7 6 5 4 3 2 1 Blastoff!
	'''

När en funktion anropar sig själv skapas en ny instans av funktionen. Det innebär att alla lokala variabler i den rekursiva funktionen är lokala till varje instans. Bara för att det är samma funktion som anropas så slutar inte reglerna för isolering av variabler inte att gälla. Det är egentligen ingen skillnad mellan ett vanligt funktionsanrop och rekursion. För att förstå rekursion är det bra att utgå från basfallet och försöka förstå vad som anropar basfallet.

När man skriver en rekursiv funktion bör man göra lagom lite i varje iteration. Om man gör för mycket får man för många basfall.

Ett rekursivt program måste ha ett basfall. Annars kommer programmet inte att terminera. I varje iteration måste man komma närmare basfallet.

Rekursion är ett sätt att göra samma sak många gånger. Ett annat sätt att åstadkomma samma sak är en *loop*. Det är dock mindre intressant.

    def factorial(n):
	    if n == 0:
		    return 1
	    else:
		    return n * factorial(n - 1)

## Interaktivt program

Om man vill läsa in data till ett program kan man använda funktionen `raw_input()`.

    def testInput():
	    print "Mata in: ",
		s = raw_input()
		print "Du skrev: ", s

`raw_input()` returnerar en sträng medan `input()` returnerar ett heltal. Detta är bra för input och till att pausa programmet för debug.

## Returvärden

Om man vill att en funktion ska returnera ett värde skriver man värdet efter `return`. Det kan se ut som `return variabel`. När interpreteraren når den raden kommer den omedelbart att återgå till anroparen och meddela return-värdet. Detta bör *inte* kombineras med en eventuell utskrift.

En funktion kan innehålla flera `return`-satser, men interpreteraren kommer bara att nå en sats per anrop.
