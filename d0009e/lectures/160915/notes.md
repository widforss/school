# Föreläsning 2016-09-15

## Inkrementell programutveckling

Hur bygger man större funktioner än de enkla vi har gjort hittills? Det gör man genom att lösa problemet i små steg.

Hitta rätt syn på problemet. Ska programmet utföra en beräkning som vi vill upprepa med olika indata. Isåfall vill vi ha en funktion.

Vad är programmets indata? Dessa blir funktionens parametrar. I värt fall vill vi kunna köra beräkningen med olika värden på `x1`, `x2`, `y1` och `y2`.

    def distance(x1, y1, x2, y2):

Efterssom programmets resultat ska vara ett decimaltal kan vi bygga en grov approximation till vår funktion genom att helt enkelt returnera värdet 0.0.

    def distance(x1, y1, x2, y2):
        return 0.0

Vi vill börja med att få något slags delresultat.

    def distance(x1, y1, x2, y2):
        dx = x2 - x1
        dy = y2 - y1
        print "dx is", dx
        print "dy is", dy
        return 0.0

Därefter närmar vi oss resultatet än mer.

    def distance(x1, y1, x2, y2):
        dx = x2 - x1
        dy = y2 - y1
        dsquared = dx*dx + dy*dy
        #print "dx is", dx
        #print "dy is", dy
        print "dsquared is", dsquared
        return 0.0

Nu har vi nästan en färdig funktion.

    import math

    def distance(x1, y1, x2, y2):
        dx = x2 - x1
        dy = y2 - y1
        dsquared = dx*dx + dy*dy
        #print "dx is", dx
        #print "dy is", dy
        #print "dsquared is", dsquared
        result = math.sqrt(dsquared)
        return result

Till sist kan vi ta bort våra spårutskrifter

    import math

    def distance(x1, y1, x2, y2):
        dx = x2 - x1
        dy = y2 - y1
        dsquared = dx*dx + dy*dy
        result = math.sqrt(dsquared)
        return result

## Boolska funktioner

Ofta kan man vara intresserad av att bara få svar på en Ja/Nej-fråga. Dessas namn börjar ofta med `is`.

    def isDivisible(x, y)
        return x%y == 0

## Turingkomplett

Nu har vi kommit såpass långt i kursen att vi kan göra alla beräkningar som en dator kan genomföra. Det kallas att språket vi använder är *turingkomplett*.

## Strängar

`"hej hopp"` är en sträng. Det finns också andra tecken man kan skriva in i strängar, t.ex. radbrytningar och tabbar, `"\n"` och `"\t"`.

## Ett större exempel

Vi vill ha ett program som skriver ut multiplikationstabeller. Det ska vara ett generellt program och vara inkapslat.

Vi skriver ut multipler av 2

    i = 1
    while i <= 6 :
        print 2*i, '\t',
        i = i + 1 
    print

Låt oss att kapsla in det i en funktion

def printRow(n):
    i = 1
    while i <= 6:
        print n*i, '\t',
        i = i + 1 
    print

Gör en tabell och stoppa den i en funktion

    def printRow(n):
        i = 1
        while i <= 6:
            print n*i, '\t',
            i = i + 1 
        print

    def printTable(n):
        i=1
        while i <= 6:
            printRow(i)

Generalisera printRow så att antalet kolumner blir `high`

    def printRow(n):
        i = 1
        while i <= 6:
            print n*i, '\t',
            i = i + 1 
        print

    def printTable(high):
        i=1
        while i <= high:
            printRow(i)
            i = i+1

Generalisera printTable så att antalet kolumner också blir `high`
    
    def printRow(n, high):
        i = 1
        while i <= high:
            print n*i, '\t',
            i = i + 1 
        print

    def printTable(high):
        i=1
        while i <= 6:
            printRow(i, high)
            i = i+1

Genom att ha ett välstrukturerat program kan man enkelt ändra i specifikationen. Till exempel, tänk om man inte vill ha några dubletter.
        
# Poänger med funktioner

* Ger möjlighet att sätta illustrativa namn på satslistor
* Undviker duplicering av kod
* Möjliggör modulariserad utveckling och testning
* Kapslar in variabler genom att göra dem lokala
* Förtydligar vilka variabler som är indata genom att göra dem till parametrar
* Ger möjlighet till rekursion
