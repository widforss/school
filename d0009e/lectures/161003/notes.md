# Föreläsning 2016-10-03

## Formateringsoperatorer

En formateringsoperator är ett sätt att stoppa in saker i strängar.

    "In %d days we made %f million %s"\
    %
    (34, 6.1, "dollars")

I Python används procenttecknet. Inuti strängen skriver man platshållare. De varierar beroende på typ. `%d`, `%f` och `%s` används vid heltal, flyttal och strängar. Det finns fler för de olika typerna. *Efter strängen måste man skriva ett procenttecken (se exemplet ovan).* Därefter definierar man vilka värden som ska stoppas in i strängen. Väl i strängen konverteras allt naturligtvis till strängar.

För att reservera ett minst antal tecken för platshållaren kan man stoppa in en siffra efter procenttecknet. `"%6d" % 62` ger `"    62"`. Däremot blir `"%6d" % 6200000` till `"6200000"`, dvs platshållaren kan ta mer plats än specifierat. `"%-6d"` blir vänsterjusterat. På flyttal kan man ange totalt antal tecken (inklusive punkt och decimaler) samt antal decimaler, såsom `"%12.2f"`.

## Spara data mellan körningar

Så länge man inte bryr sig om vilket filformatet blir kan man använda modulen *pickle*. Detta är bara för data som ska användas senare i samma program, inte för något som ska exporteras.

För att spara värdet `x` i filen `f`:

    pickle.dump(x, f)

För att använda datan igen:


    x = pickle.load(f)

## Undantag

När något går fel i Python stannar programmet vanligtvis och skriver ut ett felmeddelande. För att få programmet att istället hantera felet och gå vidare använder undantagshantering.

    try:
        satslista
    except namn1:
        satslista
    except namn2:
        satslista

Under `try:` skriver man det programmet vanligtvis ska göra. Under `except namn1:` skriver man därefter vad som ska hända om man får undantaget `namn1`.
