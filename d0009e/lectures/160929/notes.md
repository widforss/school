# Föreläsning 2016-09-29

Hur beräknar man Fibonacci på ett bättre sätt?

    def fibonacci(n):
        results = {0:1, 1:1}
        #res = fib_rec(results, n)
        #print results
        #return res
        return fib_rec(results,n)

    def fib_rec(previous, n):
        #print "Value:",n,
        #print "Dictionary:",previous
        if not previous.has_key(n):
            previous[n] = fib_rec(previous, n-1) + fib_rec(previous, n-2)
        return previous[n]

`def fibonacci(n)` är en så kallad *wrapper*. Den gör inte så mycket förutom att dölja en arbetsvariabel för användaren.

En `int` är ett tal inom intervallet -2³¹ - 2³¹. Behöver man större eller mindre tal än så kommer Python-interpreteraren att använda så kallade *långa heltal*. Dessa kan bli hur stora som helst men är långsammare och kräver mer minne än ett vanligt heltal.

# Filer

Filer används för att spara data mellan exekveringar. De ligger på ett filsystem, som är ett hierarkist system. Varje fil har ett namn och ett innehåll.

När man öppnar en fil kan man antingen skriva eller läsa.

    f = open("test.dat", "w")
    f.write "text"
    f.write "mer text"
    f.close

Notera att `"w"` **raderar tidigare innehåll**. Filen `test.dat` *måste finnas*, annars får man ett RunTimeError.

Om man använder `"r"` öppnar man istållet filen för läsning.

    f = open("test.dat", "r")
    text = f.read()
    print text
    f.close

    ''' output:
    textmer text
    '''

`readlines()` ger kvarvarande rader som en lista. Den funktionen är bara en bra idé att använda på en textfil.
