# coding=utf-8

def kostnad(P, r, a):
    ''' Calculates the total cost of a loan
    P (float), initial size of the loan
    r (float), yearly interest rate
    a (float >= 0), years until loan is paid
    '''
    P = float(P)
    r = float(r)
    a = float(a)

    # I don't think this formula works backwards in time.
    if a < 0:
        print "Bad parameter 'a', may not be negative."
        return
    
    cost = P + (a + 1) * P * r / 2
    cost = int(round(cost))

    print "Den totala kostnaden efter", a, "år är", cost, "kr."

kostnad(50000, 0.03, 10)
