# coding=utf-8

def sockerkaka(antal):
    if int(antal) <= 0:
        print "Bad parameter 'antal'."
    else:
        # standard (integer > 0), number of people for base recipe.
        standard = 4
        # discrete (list[ unit (string) ]), list of discrete units.
        discrete = ["", "st"]
        # recipe (list[
        #           list[
        #             ingredient (string),
        #             unit (string),
        #             amount (float/int)
        #           ]
        #         ])
        recipe = [["smör",          "g",    85],
                  ["ströbröd",      "dl",    0.75],
                  ["ägg",           "st",    3],
                  ["strösocker",    "dl",    3],
                  ["vaniljsocker",  "tsk",   2],
                  ["bakpulver",     "tsk",   2],
                  ["vetemjöl",      "dl",    3],
                  ["vatten",        "dl",    1]]

        for ingredient in recipe:
            # Scale and round ingredients.
            amount = float(ingredient[2]) * int(antal) / standard
            if ingredient[1] in discrete:
                amount = int(round(amount, 0))
            else:
                amount = round(amount, 2)

            print amount, ingredient[1], ingredient[0]

sockerkaka(4)
sockerkaka(7)
