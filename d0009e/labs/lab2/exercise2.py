# coding=utf-8

def bounce2(n):
    ''' Prints values "n n-1 n-2 ... 0 1 2 ... n" given natural number n.
    '''
    intn = int(n)
    escalator = intn
    # We only want natural numbers.
    if intn < 0:
        print "Bad value 'n'"

    # Print the absolute value of escalator all the way down to -intn
    while escalator >= -intn:
        print abs(escalator),
        escalator -= 1

bounce2(4)
