# coding=utf-8

def bounce(n):
    ''' Prints values "n n-1 n-2 ... 0 1 2 ... n" given natural number n.
    '''
    intn = int(n)
    # We only want natural numbers.
    if intn < 0:
        print "Bad value 'n' < 0:", intn

    print intn,
    ''' Call bounce(n) recursively all the way down to 0 and 
        print n on the way out
    '''
    if intn > 0:
        bounce(intn - 1)
        print intn,

if __name__ == "__main__":
    bounce(4)
    print ''
    bounce(7)
    print ''
    bounce(0)
