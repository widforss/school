# coding=utf-8
import math

def derivate(f, x, h):
    ''' Returns the approximate derivate of f(x).
    f (function)
    x (float), x value of f(x)
    h (float), limit value for approximating f'(x)
    '''
    x = float(x)
    h = float(h)

    derivate = (f(x + h) - f(x - h))/(2 * h)
    return derivate

def line(x):
    y = x
    return y

def square(x):
    y = x ** 2
    return y


#print derivate(line, 2, 0.001)
#print derivate(square, 0, 0.001)
#print derivate(math.sin, math.pi, 0.001)


def solve(f, x0, h):
    ''' Solve f(x) = 0 using Newton-Raphson's method given
    f (function)
    x0 (float), start x-value
    h (float), precision
    '''
    x = float(x0)
    last = x
    h = abs(float(h))
    start = True

    # Approximate using Newton-Raphson's method
    # Continue until ingoing and outgoing values differs less than h
    while abs(x - last) > h or start == True:
        start = False
        last = x
        x = x - f(x) / derivate(f, x, h)
    return x

def square_minus(x):
    y = x ** 2 - 1
    return y

def exponent_minus(x):
    y = 2 ** x - 1
    return y

def e_exponent(x):
    y = x - math.e ** -x
    return y

if __name__ == "__main__":
    print solve(square_minus,   -220, 0.00000001)
    print solve(exponent_minus, 10, 0.00000001)
    print solve(e_exponent,     10, 0.00000001)
