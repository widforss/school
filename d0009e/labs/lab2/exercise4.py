# coding=utf-8

def tvarsumma2(n):
    ''' Calculates sum of all digits in a real integer n.
        summa should not be supplied by original caller.
    '''
    n = int(n)
    # summa (int), sum of digits in n.
    summa = 0
    # We only want positive integers
    if n < 0:
        print "Bad argument 'n' < 0."
        return

    # Divide n by ten and add rest to summa. Continue until n is less
    # than 10
    while n >= 10:
        difference = divmod(n, 10)
        summa += difference[1]
        n = difference[0]

    # Add most significant digit in n (which was left in intn after the loop)
    summa += n
    print summa

tvarsumma2(50545)
