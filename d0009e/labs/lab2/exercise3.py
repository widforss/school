# coding=utf-8

def tvarsumma(n, summa=0):
    ''' Calculates sum of all digits in a real integer n.
        summa should not be supplied by original caller.
    '''
    n = int(n)
    summa = int(summa)
    # We only want positive integers
    for parameter in n, summa:
        if parameter < 0:
            print "Bad argument '", parameter, "' < 0."
            return

    # Modulus by 10 and add rest to summa. Repeat recursively until quota is 0.
    difference = divmod(n, 10)
    summa += difference[1]
    if difference[0] <= 0:
        print summa
    else:
        tvarsumma(difference[0], summa)

if __name__ == "__main__":
    tvarsumma(0)
