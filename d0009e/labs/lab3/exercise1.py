# coding=utf8

import exercise0

def insertWord(wordlist):
    ''' Asks for a word and it's description. If the word exists in wordlist,
        returns false, otherwise a tuple with word and description.
    wordlist (list[string()]): List of already existing words.
    '''
    word  = raw_input("Word to insert: ")
    if word in wordlist:
        print "Word already exists."
        return False
    descr = raw_input("Description of word: ")
    return(word, descr)

def lookupWord(wordlist):
    ''' Asks for a word and checks if it exists in wordlist. If it does not
        returns false, otherwise returns tuple of word and None.
    wordlist (list[string()]): List of already existing words.
    '''
    word  = raw_input("Word to lookup: ")
    if not word in wordlist:
        print "Word does not exist."
        return False
    print "Description of", word, ":",
    # Return None in second element to be consistent with insertWord().
    return (word, None)

def actionMenu():
    ''' Menu for performing operations on a wordlist.
    '''
    menu_string = ("\nMenu for dictionary,\n\n"
                   "[1] Insert\n"
                   "[2] Lookup\n"
                   "[0] Exit program\n"
                   "----")
    while True:
        print menu_string
        try:
            choice = int(raw_input("Choose menu item: "))
        except ValueError:
            exercise0.valueErr()
            continue
        if   choice == 1:
            return "insert"
        elif choice == 2:
            return "lookup"
        elif choice == 0:
            return "exit"
        else:
            print "Chosen action does not exist."
        

def word_list():
    ''' Presents a menu of operations to perform on a wordlist. It is possible
        to add and look up words.

        The list consists of two parallel list, one with the words and one with
        the definitions.
    '''
    wordlist = [[],[]]
    # We don't want to bother external functions with the format of our list
    words_only = wordlist[0]

    while True:
        action = actionMenu()

        if action == "insert":
            # Ask for word, description and check that word does not exist.
            word = insertWord(words_only)
            if word == False:
                continue                
            wordlist[0] += [word[0]]
            wordlist[1] += [word[1]]
            print wordlist
        elif action == "lookup":
            # Ask for word and make sure it exists in wordlist
            word = lookupWord(words_only)
            if word == False:
                continue                
            index_of_word = wordlist[0].index(word[0])
            print wordlist[1][index_of_word]
        elif action == "exit":
            return
        else:
            raise ValueError, ('Action is not defined')

def word_tupl():
    ''' Presents a menu of operations to perform on a wordlist. It is possible
        to add and look up words.

        The list consists of a list of tuples, where the first element is the
        word and the second is the definition.
    '''
    wordlist = []

    while True:
        # We don't want to bother external functions with the format of our list
        words_only = [words[0] for words in wordlist]
        action = actionMenu()

        if action == "insert":
            # Ask for word, description and check that word does not exist.
            word = insertWord(words_only)
            if word == False:
                continue                
            wordlist += [word]
            print wordlist
        elif action == "lookup":
            # Ask for word and make sure it exists in wordlist
            word = lookupWord(words_only)
            if word == False:
                continue                
            wordmatches = [pair for pair in wordlist if pair[0] == word[0]]
            print wordmatches[0][1]
        elif action == "exit":
            return
        else:
            raise ValueError, ('Action is not defined')

def word_dict():
    ''' Presents a menu of operations to perform on a wordlist. It is possible
        to add and look up words.

        The list consists of a dictionary, where the key is the word and the
        value is the definition.
    '''
    wordlist = {}

    while True:
        # We don't want to bother external functions with the format of our list
        words_only = wordlist.keys()
        action = actionMenu()

        if action == "insert":
            # Ask for word, description and check that word does not exist.
            word = insertWord(words_only)
            if word == False:
                continue                
            wordlist[word[0]] = word[1]
            print wordlist
        elif action == "lookup":
            # Ask for word and make sure it exists in wordlist
            word = lookupWord(words_only)
            if word == False:
                continue                
            print wordlist[word[0]]
        elif action == "exit":
            return
        else:
            raise ValueError, ('Action is not defined')

if __name__ == "__main__":
    menu_string = ("\n[1] word_list()\n"
                   "[2] word_tupl()\n"
                   "[3] word_dict()\n"
                   "[0] terminate\n"
                   "----")

    # options[n][1]():  function to call on menu.
    # options[n][2]:    Question to ask on menu action.
    # options[n][3]:    list of argument to send to function
    menu_opts = {1: (word_list, ""),
                 2: (word_tupl, ""),
                 3: (word_dict, "")
    }

    exercise0.menu(menu_string, menu_opts)
