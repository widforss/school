# coding=utf8

# Imports copies of exercises in lab3 to be chosen in menu().
import bounce
import tvarsumma
import newton

def toSolve(x):
    ''' Function to solve in menu_solve.
    '''
    return x**2 - 1

def valueErr():
    print "Please enter an integer instead"

def menu(menu_str, options):
    ''' Prints a menu, ask a subsequent question on menu action and calls
        the chosen function. The called function runs `input()` or something
        else to get answer to question (not our problem).
        
        The menu position `0` will always terminate after running the
        specified (or unspecified) `0`-value in `options`.

    menu (str): String consisting of all menu options in human readable format.
    options (dict(int: tupl(function, str, list[optional arguments]))): 
        int:        Identifier for menu item. Input this int to chose item.
        function:   function to call after asking question.
        str:        string to print before calling function.
        arguments:  a list of arguments to send to function
    '''
    choice = None
    # Let's not run the whole dictionary thing on "[0] terminate".
    while choice != 0:
        print menu_str
        # Stupid user will not enter valid input.
        try:
            choice = int(raw_input("Choose menu item: "))
        except ValueError:
            valueErr()
            continue
        # options[n][1]: Question to ask on menu action.
        try:
            print options[choice][1]
        except KeyError:
            # Skip everything if menu item does not exist
            continue

        while True:
            # options[n][0](): function to call on menu.
            if len(options[choice]) < 3:
                options[choice][0]()
            else:
                options[choice][0](*options[choice][2])
            break 

def menu_bounce():
    ''' Takes input for bounce.bounce(n) and then bounces. *oh yeah!*
    '''
    try:
        n = int(raw_input("n = "))
    except ValueError:
        valueErr()

    if n >= 0:
        # TODO: Will reach max recursion depth at large number `n`.
        bounce.bounce(n)
        print

def menu_tvarsumma():
    ''' Takes input for tvarsumma.tvarsumma(n) and then calculates digit sum.
    '''
    try:
        n = int(raw_input("n = "))
    except ValueError:
        valueErr()

    if n > 0:
        tvarsumma.tvarsumma(n)
        print

def menu_solve():
    ''' Takes starting guess for newton.solve() and then solves `toSolve`.
    '''
    try:
        x0 = int(raw_input("x₀ = "))
    except ValueError:
        valueErr()

    # TODO: Will reach max recursion depth at large number `n`.
    print newton.solve(toSolve, x0, 0.0000000000001)

if __name__ == "__main__":
    menu_string = ("[1] bounce(n)\n"
                   "[2] tvarsumma(n)\n"
                   "[3] solve(f(x)=x²-2, x0, 0.0000001)\n"
                   "[0] terminate\n"
                   "----")

    # options[n][0]():  function to call on menu.
    # options[n][1]:    Question to ask on menu action.
    menu_opts = {1: (menu_bounce, "Input natural number to bounce."),
                 2: (menu_tvarsumma, "Input natural number to check digit sum."),
                 3: (menu_solve, "Guess solution to equation 0=x²-2.")
    }

    menu(menu_string, menu_opts)
