# coding=utf-8

class NoSuchEntry(Exception):
    pass

class EntryAlreadyExists(Exception):
    pass

class SemicolonInInput(Exception):
    pass

class WrongNumberOfArgs(Exception):
    pass

class WrongNumber(Exception):
    pass

class NeedNumber(Exception):
    pass

class PhoneBook:
    ''' Keeps a list of PhoneEntry-objects and performs the instructions
            add     - adds a PhoneEntry object with a number and an alias
            lookup  - takes an alias and returns the number
            alias   - adds an alias to a PhoneEntry object
            change  - changes the number of a PhoneEntry object given an alias
            save    - saves the current PhoneBook to a semicolon-separated file
            load    - loads a semicolon-separated file to PhoneBook
    '''
    def __init__(self):
        ''' This list would be the actual phoneBOOK.
            It will be filled with PhoneEntry objects
        '''
        self.personas = []

    def addPersona(self, alias, number):
        ''' Adds an instance of PhoneEntry to self.personas given a name and
            a number.
        Raises:
            EntryAlreadyExists on duplicate number.
            SemicolonInInput if semicolon in name or number.
        '''
        if self.existsNumber(number):
            raise EntryAlreadyExists("Number already exists in PhoneBook.")
        else:
            self.personas += [PhoneEntry(alias, number)]

    def lookupAliasNumber(self, alias):
        ''' Returns list of numbers matching specified alias.
        Raises:
            NoSuchEntry if the alias does not exist.
        Returns:
            List of numbers (list of strings)
        '''
        indexes = self.lookupAliasIndexes(alias)
        numbers = []
        for index in indexes:
            numbers += [self.personas[index].returnnumber()]
        return numbers
                
    def addAlias(self, oldAlias, newAlias, number=False):
        ''' Adds alias to persona in PhoneBook. Will need number
            specified if the alias is used in multiple entries.
        Raises:
            NoSuchEntry if the old alias does not exist.
            NeedNumber if the number is needed to complete.
            SemicolonInInput if semicolon in new alias.
            WrongNumber if the specified number is wrong
            NeedNumber if a number is needed
        '''
        indexes = self.lookupAliasIndexes(oldAlias)
        # If a number is specified, check that it is correct and use it.
        if number:
            try:
                numberIndex = self.lookupNumberIndex(number)
            except NoSuchEntry:
                raise WrongNumber()
            # Check that number match is also an alias match
            if numberIndex in indexes:
                self.personas[numberIndex].addAlias(newAlias)
            else:
                raise WrongNumber()
        # If no number specified, check if we don't need it.
        elif len(indexes) == 1:
            # If there is only one alias match, we go and add the new alias.
            self.personas[indexes[0]].addAlias(newAlias)
        # Go get that number.
        else:
            raise NeedNumber()

    def changeNumber(self, alias, number, oldnumber=False):
        ''' Changes number in PhoneEntry that matches alias. Will need
            oldnumber if the alias exists in multiple entries.
        Raises:
            NoSuchEntry if there is no alias match.
            EntryAlreadyExists if the new number already exists.
            SemicolonInInput if there is a semicolon in number.
            WrongNumber if the specified number is wrong
            NeedNumber if a number is needed
        '''
        if self.existsNumber(number):
            raise EntryAlreadyExists("Number already exists in PhoneBook")
        indexes = self.lookupAliasIndexes(alias)
        # If oldnumber is specified, check if it's correct and use it.
        if oldnumber:
            try:
                oldnumberindex = self.lookupNumberIndex(oldnumber)
            except NoSuchEntry:
                raise WrongNumber()
            if oldnumberindex in indexes:
                self.personas[oldnumberindex].changeNumber(number)
            else:
                raise WrongNumber()
        # If oldnumber is not specified, check if we can do without.
        elif len(indexes) == 1:
            self.personas[indexes[0]].changeNumber(number)
        # Go get that number.
        else:
            raise NeedNumber()

    def remove(self, alias, number=False):
        ''' Removes entry. Needs number if alias exists elsewhere.
        Raises:
            NoSuchEntry if alias does not exist
            WrongNumber if number is incorrect
            NeedNumber if number is needed
        '''
        indexes = self.lookupAliasIndexes(alias)
        if number:
            try:
                numberIndex = self.lookupNumberIndex(number)
            except NoSuchEntry:
                raise WrongNumber
            if numberIndex in indexes:
                del self.personas[numberIndex]
            else:
                raise WrongNumber()
        elif len(indexes) == 1:
            del self.personas[indexes[0]]
        else:
            raise NeedNumber()

    def exportPhoneBook(self, filename):
        ''' Prints all data in currently loaded PhoneBook to filename.
        '''
        export = []
        for persona in self.personas:
            export += [persona.exportLine()]
        file = InOutFile(filename)
        file.writeData(export)


    def importPhoneBook(self, filename):
        ''' Loads data saved in filename to PhoneBook. CLEARS all currently
            loaded data and REPLACES it with the data in filename.
        '''
        file = InOutFile(filename)
        imported = file.readData()
        self.personas = []
        for personaLine in imported:
            self.ImportedLineToPhoneBook(personaLine)

    #
    #
    # Beneath is where actual work is done in PhoneBook object.
    #
    #

    def ImportedLineToPhoneBook(self, personaLine):
        ''' Turns one export-formatted string into a PhoneEntry/persona-
            object.
        Returns:
            PhoneEntry-object
        '''
        lineList = personaLine.split(";")
        number = lineList[0]
        name = lineList[1]
        self.addPersona(name, number)
        if len(lineList) > 2:
            for alias in lineList[2:]:
                self.addAlias(name, alias, number)

    def existsNumber(self, number):
        ''' Checks if a number exists in PhoneBook.
        Returns:
            Boolean
        '''
        for persona in self.personas:
            if number == persona.number:
                return True
        return False

    def existsAlias(self, alias):
        ''' Checks if an alias exists in PhoneBook.
        Returns:
            Boolean
        '''
        try:
            self.lookupAliasIndexes(alias)
            return True
        except NoSuchEntry:
            return False

    def lookupAliasIndexes(self, alias):
        ''' Searches for an alias in PhoneBook's entries and returns
            a list of indexes that match the alias.
        Raises:
            NoSuchEntry if there is no match.
        Returns:
            Index (Integer)
        '''
        indexes = []
        for index, persona in enumerate(self.personas):
            if persona.existsAlias(alias):
                indexes += [index] 
        # If we're through with no match, the alias does not exist.
        if not indexes == []:
            return indexes
        else:
            raise NoSuchEntry("No persona with that alias in PhoneBook.")

    def lookupNumberIndex(self, number):
        ''' Searches for a number in PhoneBook's entries and returns
            the first index that match the number.
        Raises:
            NoSuchEntry if there is no match.
        Returns:
            Index (Integer)
        '''
        for index, persona in enumerate(self.personas):
            if persona.returnnumber() == number:
                return index
        # If we're through with no match, the number does not exist.
        raise NoSuchEntry("No persona with that number in PhoneBook.")

class PhoneEntry:
    ''' One entry in PhoneBook containing a number and at least one alias.
        Performs the real work for some PhoneBook methods to keep the internal
        representation hidden from PhoneBook.
    '''
    def __init__(self, alias, number):
        ''' The number is stored as a plain string and the aliases are kept in
            aliasList. There is no primary alias, they are all equal. There are
            no formatting controls on either value, except no semicolons.
        Raises:
            SemicolonInput if semicolon in number or alias.
        '''
        for input in (alias, number):
            self.checkSemicolon(input)
        self.aliasList  = [alias]
        self.number = number
    
    def addAlias(self, newAlias):
        ''' Adds an alias (as in the "alias" command)
        '''
        self.checkSemicolon(newAlias)
        self.aliasList += [newAlias]

    def changeNumber(self, number):
        ''' Changes the number (as in the "change" command)
        '''
        self.checkSemicolon(number)
        self.number = number

    def existsAlias(self, alias):
        ''' Checks if alias exists
        '''
        return alias in self.aliasList

    def returnnumber(self):
        ''' Returns the number
        '''
        return self.number

    def exportLine(self):
        ''' Turns one PhoneEntry/persona-object into an export-formatted string
        Returns:
            export-formatted string
        '''
        line = str(self.number) + ";"
        for alias in self.aliasList:
            line += str(alias) + ";"
        return line


    def checkSemicolon(self, input):
        ''' Checks input for semicolons (forbidden due to semicolon separated
            export).
        '''
        if ";" in input:
            raise SemicolonInInput("Semicolon in input: " + str(input))

class InOutFile:
    ''' Handles all file write/read and converts from list to file and back.
    '''
    def __init__(self, fileName):
        ''' Filename to write to and to read from.
        '''
        self.fileName = fileName

    def writeData(self, data):
        ''' Takes a list of export-formatted lines and writes them to fileName
            as lines.
        '''
        f = open(self.fileName, 'w')
        for line in data:
            f.write(line + "\n")
        f.close()

    def readData(self):
        ''' Reads fileName and returns it as export-formatted list
        Returns:
            Export-formatted list
        '''
        data = []
        f = open(self.fileName)
        for line in f:
            if line != "\n":
                data += [line[:-1]]
        f.close()
        return data

class UserInterface:
    ''' Handles all user interface and user-facing exception handling
    '''
    def __init__(self):
        ''' Initializes the PhoneBook instance the user will interact with and
            specifies some error messages.
        '''
        self.phoneBook = PhoneBook()
        self.argError = {"add":    "add syntax: name number",
                         "lookup": "lookup syntax: name",
                         "alias":  "alias syntax: name newname [number]",
                         "change": "change syntax: name newnumber [number]",
                         "remove": "remove syntax: name [number]",
                         "save":   "save syntax: filename.",
                         "load":   "load syntax: filename."}
        self.notvalid = " is not a valid command."
        self.allError = {"NoSuchEntry": "Value does not exist: ",
                         "EntryAlreadyExists": "The value already exists: ",
                         "SemicolonInInput": "Semicolon is not allowed.",
                         "WrongNumber": "The specified number is incorrect: ",
                         "NeedNumber": "You need to specify current number."}

    def prompt(self):
        ''' The actual prompt that the user will see. Checks the input and
            sends it in the right direction. The loop breks on user input
            "quit".
        '''
        while True:
            input = raw_input(">")
            if input == "":
                continue
            command = input.split()
            # Let's split the command into function and arguments
            if command != []:
                function = command[0]
            else:
                continue
            if len(command) > 1:
                arguments = command[1:]
            else:
                arguments = []

            try:
                if   function == "add":
                    self.add(*arguments)
                elif function == "lookup":
                    self.lookup(*arguments)
                elif function == "alias":
                    self.alias(*arguments)
                elif function == "change":
                    self.change(*arguments)
                elif function == "remove":
                    self.remove(*arguments)
                elif function == "save":
                    self.save(*arguments)
                elif function == "load":
                    self.load(*arguments)
                elif function == "quit":
                    return
                elif function != "":
                    print function + " is not a valid command."
            except WrongNumberOfArgs:
                print self.argError[function]
            except NoSuchEntry:
                print self.allError["NoSuchEntry"] + arguments[0]
            except SemicolonInInput:
                print self.allError["SemicolonInInput"]
            except WrongNumber:
                print self.allError["WrongNumber"] + arguments[2]
            except NeedNumber:
                print self.allError["NeedNumber"]
            except EntryAlreadyExists:
                print self.allError["EntryAlreadyExists"] + arguments[1]

    def add(self, *args):
        if len(args) != 2:
            raise WrongNumberOfArgs
        self.phoneBook.addPersona(*args)

    def lookup(self, *args):
        if len(args) != 1:
            raise WrongNumberOfArgs
        for number in self.phoneBook.lookupAliasNumber(*args):
            print number

    def alias(self, *args):
        if not 2 <= len(args) <= 3:
            raise WrongNumberOfArgs
        self.phoneBook.addAlias(*args)

    def change(self, *args):
        if not 2 <= len(args) <= 3:
            raise WrongNumberOfArgs
        self.phoneBook.changeNumber(*args)

    def remove(self, *args):
        if not 1 <= len(args) <= 2:
            raise WrongNumberOfArgs
        self.phoneBook.remove(*args)

    def save(self, *args):
        if len(args) != 1:
            raise WrongNumberOfArgs
        self.phoneBook.exportPhoneBook(*args)

    def load(self, *args):
        if len(args) != 1:
            raise WrongNumberOfArgs
        file = InOutFile(*args)
        try:
            self.phoneBook.importPhoneBook(*args)
        except IOError:
            print "File " + args[0] + " does not exist."
        except EntryAlreadyExists:
            self.phoneBook = PhoneBook()
            print "File contains multiple identical numbers. Memory cleared."

if __name__ == "__main__":
    ui = UserInterface()
    ui.prompt()
