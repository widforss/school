/*
 * game.c
 *
 *  Created on: 31 aug 2014
 *      Author: H�kan Jonsson
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(void) {

	int low = 1, high = 50, guess, number, num = 0;

	printf("Welcome to NGG, the Number Guessing Game!\n\n");
	printf("What number, between %d and %d, am I thinking of?\n", low, high);

	srand(time(NULL));
	number = rand() % (high - low + 1) + low;

	do {
		printf("Enter%s guess: ", (num == 0 ? "" : " another"));
		if (scanf("%d", &guess) == 1) {
			num++;
			if (guess < number) {
				printf("That's too low... ");
			} else if (guess > number) {
				printf("That's too high... ");
			}
		} else {
			printf("That's not a valid guess and doesn't count... ");
		}
		while (getchar() != '\n')
			;
	} while (guess != number);
	printf("That's correct! It took you %d guess%s.\n\nBye!\n", num,
			(num == 1 ? "(!)" : "es"));
}
