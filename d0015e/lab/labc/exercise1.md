# Min första läsvecka

Min första läsvecka har ännu inte nått sitt slut. I skrivande stund har jag haft två föreläsningar i kursen *d0009e* samt två föreläsningar och två prov i kursen *d0015e*. Just nu har jag laboration i den sistnämnda kursen. Det har gått bra hittills och jag känner att jag hänger med i det som undervisas.

I onsdags kväll var jag på spex. Efter spexet råkade jag krossa min pressbryggare medan jag tömde den, något som jag hade glömt att göra tidigare under dagen. Det gjorde att jag skar mig rätt ordentligt i lillfingret. Det var lite tröttsamt eftersom jag precis hade tänkt gå och lägga mig.

På torsdagsmorgonen hade jag ett muntligt prov i *d00195e*. Det försov jag mig till. Lyckligtvis fick jag gå in med en grupp som skulle gå in när jag kom.

På eftermiddagen köpte jag en taklampa. Den passade inte, så jag har fortfarande inte någon taklampa hemma i Kårhuset. Lite smått irriterad över detta ägnade jag kvällen åt att spela Portal 2. Det var roligt.

Idag har jag två föreläsningar och två laborationer. I *d0015e* håller vi på att lära oss Unix. Det är intressant. Nu har jag skrivit 201 ord enligt `wc`.
