\documentclass[a4paper,12pt]{article}
\usepackage[swedish]{babel}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{verbatim}
\usepackage{amsmath}
\newenvironment{myverbatim}{\verbatim}{\endverbatim}

\title{Skidbackar och deras lutning}
\author{Aron Widforss\thanks{email: \texttt{arowid-6@student.ltu.se}}\\  
            ~ \\ Luleå tekniska universitet \\ 
            971 87 Luleå, Sverige}
\date{\today}

\begin{document}

\maketitle

\begin{abstract}
  
  Ingen skidbacke är den andra lik. De har olika lutning och får därav
  olika åkegenskaper. Vissa skidbackars profiler går enkelt att uttryckas
  som en matematisk funktion. Den här rapporten tar upp en typ av sådana
  backar, en specifik backe och en allmän.

  Genom att undersöka backarnas \emph{derivata} och \emph{andraderivata}
  undersöks backarnas lutning vid olika positioner, bland annat bestäms var
  backarna kommer att vara som brantast.
\end{abstract}

\section{Introduktion}

  Givet en profil av en skidbacke och en matematisk beskrivning av denna är
  vi intresserade av backens egenskaper. Detta kan vara intressant inför
  prospektering av nya backar, då backens profil och egenskaper spelar stor
  roll för åkglädjen.

  Genom att beskriva backen matematiskt får vi möjlighet att bedöma backars
  lämplighet även om dess exakta fallhöjd och lutning till synes skiljer
  sig från den givna förebilden.
  
\section{Uppgift och lösning}

  En skidbackes profil kan beskrivas enligt figur~\ref{fig:profil}.
  %
  \begin{figure}[tbh]
    \centering
    \includegraphics[width=9cm]{profil.png}
    \caption{Backens profil kan beskrivas med ovanstående graf.}
    \label{fig:profil}
  \end{figure}
  %
  Matematiskt kan backens höjd $y$ km uttryckas som en funktion av sträckan
  $x$ km från toppen,
  %
  \begin{equation}
    \label{eq:backe}
    y = 0{,}5 e^{-x^2} ;~ 0 \leq x \leq 2{,}5,
  \end{equation}
  %
  medan en allmän funktion för den här typen av backar kan beskrivas
  %
  \begin{equation}
    \label{eq:allmanbacke}
    y = 0{,}5 e^{-ax^2} ;~ 0 \leq x \leq 2{,}5 ;~ 0 < a.
  \end{equation}

  Den här rapporten utreder tre egenskaper hos dylika backar. Först
  undersöks vad för lutning, \emph{derivata}, som backen i
  figur~\ref{fig:profil} har 800 meter i $x$-led från toppen.
  Därefter beskrivs en funktion som bestämmer $x$-värdet för den
  brantaste punkten i backar som kan beskrivas med den allmänna
  funktionen~\ref{eq:allmanbacke}. Till
  slut utreds vilken backe som dels kan beskrivas med den allmänna
  funktionen, dels har sin brantaste punkt precis 1000 meter i $x$-led från
  backens topp.

  \subsection{Backens lutning vid $x = 0{,}8$}
  \label{sec:lutning}

  För att bestämma backens derivata vid $x = 0{,}8$ deriveras
  funktion~\ref{eq:backe} till
  %
  \begin{align*}
    y''  &= -2x \cdot 0.5 e^{-x^2} \\
         &= -x e^{-x^2},
  \end{align*}
  %
  vilket vid $x = 0{,}8$ blir
  \begin{equation*}
    y'(0{,}8) = -0{,}42.
  \end{equation*}
  %
  Det innebär att backens derivata 800 meter i $x$-led från toppen
  är $-0{,}42$.

  \subsection{Backens brantaste punkt}

  En backe är vid sin högsta punkt plan. Backens lutning ökar med avståndet
  från toppen, fram till en viss punkt, där lutningen istället avtar med
  sträckan. Backen är brantast vid den punkt där lutningen börjar avta.
  

  Backens lutning beskrivs som vi sett i avsnitt~\ref{sec:lutning} av dess
  derivata. Derivatans förändring beskrivs i sin tur av andraderivatan. När
  backen går från att bli allt brantare till att bli allt flackare kommer
  andraderivatan att gå från att vara negativ till att vara positiv. Vid
  backens brantaste punkt kommer andraderivatan alltså att vara noll.
  
  Derivatan till den allmänna funktionen~\ref{eq:allmanbacke} är
  %
  \begin{equation*}
    y' = -ax e^{ax^2}.
  \end{equation*}
  %
  Derivatan är en produkt. För att förstå vad andraderivatan är kan man
  använda produktregeln som säger att 
  %
  \begin{align*}
                 y &= f(x) g(x) \\
    \Rightarrow y' &= f'(x) g(x) + f(x) g'(x),
  \end{align*}
  vilket innebär att
  \begin{align*}
                 y' &= -ax \cdot e^{ax^2} \\
    \Rightarrow y'' &= -a \cdot e^{-ax^2} + \left( -ax \right) \left( -2ax e^{-ax^2} \right) \\
                    &= a e^{-ax^2} \left( 2ax^2 - 1 \right).
  \end{align*}
  %
  Andraderivatan är alltså en produkt är lika med noll. Eftersom
  \begin{align*}
                                a &> 0 \\
    \Rightarrow a e^{-ax^2} &> 0
  \end{align*}
  %
  kan man konstatera att
  \begin{align}
          y'' = 0 &= 2ax^2 - 1 \nonumber \\
    \Rightarrow x &= \sqrt{\frac{1}{2a}}.
      \label{eq:steepest}
  \end{align}
  %
  Där backen är brantast kommer alltså $x = \sqrt{\frac{1}{2a}}$.

  \subsection{Brantast vid $x = 1$}

  Vill man bestämma $a$ så att backens brantaste punkt hamnar vid $x = 1$
  behöver man bara sätta $x$ till nämnda värde i funktion~\ref{eq:steepest},
  %
  \begin{align*}
                    1 &= \sqrt{\frac{1}{2a}} ;~ a > 0\\
    \Leftrightarrow 1 &= \frac{1}{2a} \\
    \Leftrightarrow a &= \frac{1}{2}.
  \end{align*}

\end{document}
