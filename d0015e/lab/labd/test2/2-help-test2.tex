\documentclass[a4paper,12pt]{article}
\usepackage{amsmath, amsthm, amssymb}
\usepackage[swedish]{babel}
\usepackage{a4wide}
\usepackage[utf8]{inputenc}
\usepackage{enumerate}

\title{Hjälp till \LaTeX-prov 2}

\author{Håkan Jonsson\thanks{email: \texttt{hj@ltu.se}} \\ ~ \\ 
  Luleå tekniska universitet \\ Institutionen för systemteknik \\
  971 87 Luleå, Sverige}

\date{\today}


\begin{document}

\maketitle

\begin{abstract}
  Att skriva är att kommunicera tankar. Ska de senare nå fram måste de
  formuleras så att mottagaren förstår dem. \LaTeX\ avhjälper många
  fel och brister automatiskt men författaren styr ändå i hög grad hur
  det färdiga dokumentet blir.   

  Här går jag igenom hur man kan undvika några vanliga fel som
  studenter ibland gör då de gör prov 2 i momentet \emph{Texter
    och LaTeX} i kursen D0015E Datetknik och ingenjörsvetenskap. 
\end{abstract}


\section{Om rättningen}

Det som styr rättningen är att det färdiga dokumentet och \LaTeX-koden
är läsbara, lätta att förstå och välstrukturerade. Koden ska spegla en
sund användning av \LaTeX. Ofta skriver man inte ensam utan med
kollegor och för att samarbetet ska fungera måste man vara noga med
hur man skriver. Eftersom lösningarna på de matematiska problemen var
givna utgår rättningen inte så mycket från i vilken grad de återgetts
korrekt. Viktigt är att formler och ekvationer motiveras ordentligt.  

När en lärare bedömmer en skriftlig rapport så drar ovårdat språk och
trasigt upplägg ner betyget, och det oavsett om sakinnehållet ändå är
helt korrekt. Det finns förstås andra situationer där språket inte
spelar så stor roll, t ex där det man skriver inte ska läsas av
människor, och då är korrekt sakinnehåll det primära.  

Den som lyckas skriva kod som genererat ett dokument med vad som kan
betraktas som en lösning på Skolverkets uppgift blir godkänd. Alla
godkända grupperas efter hur bra de är och placeras i en av de tre
katagorier  
\begin{enumerate}
\item ''Godkänd'',
\item ''Ok! Godkänd'' och 
\item  ''Ok! Bra! Godkänd'' som är bäst. 
\end{enumerate}
Dessa kategorier motsvarar ungefär 50\%-66\% rätt, 67\%-83\% rätt
respektive mer än 83\% rätt. Exakt var inom respektive intervall en
viss kod/ett visst dokument hamnar går inte att säga i förväg. Det är
alldeles för stor variation för det. Men följer man det som sägs i
denna text ökar sannolikheten dramatiskt för att man ska få ett högt
betyg. 

\subsection{Språk}

Visserligen är syftet med laborationen att få inblick i \LaTeX\ men
skriver man en text så måste också språket och textens upplägg vara
vårdat. Om man inte kan skriva spelar det ingen roll vilka
skrivverktyg man har till sitt förfogande. 

Stavfel, särskrivningar, fel i meningsbyggnader och syftningsfel är
alla exempel på sånt som inte får förekomma. Visst, några småfel
dröjer sig (nästan) alltid kvar men deras antal ska vara mycket litet.  

Meningar ska avslutas med punkt. Ett vanligt fel är att punkten
utelämnas då meningen avslutas med en matematisk formel. Så ska det
inte vara. T ex så skriver man   

\begin{quote}
  ... Deriverar vi ekvation~12 får vi slutligen 
  $f^\prime(x)=x^2+2x+4.$
\end{quote}
Även då formeln inte ingår i löptexten sätts punkt sist:
\begin{quote}
  ... Deriverar vi ekvation~12 får vi slutligen 
  $$f^\prime(x)=x^2+2x+4.$$
\end{quote}
Det är ingen skillnad mellan dessa fall. En svårighet kan uppstå om
man slutar med decimaltal där man använt decimalpunkt. Det kan se
''konstigt'' ut med t ex  
\begin{quote}
  ... får vi att $b=1.0$.
\end{quote}
men så ska det alltså vara. 

\subsection{Särskrivning}

Med särskrivning menas att ett sammansatt ord skrivs som två ord efter
varandra så att meningens betydelse blir fel. I en del språk, t ex
engelskan, är särskrivning det normala men det gäller inte
svenskan. Tänk t ex på skillnaden mellan ''sjuksyster'' och ''sjuk
syster'': 
\begin{itemize}
\item \emph{Min syster är en sjuksyster [och jobbar på ett sjukhus]}.
\item \emph{Min syster är en sjuk syster [och är inlagd på sjukhus]}.
\end{itemize}
På liknande sätt måste man vara försiktig i teknisk och matematisk
text. Antag att vi just har gett två derivator $f^\prime$ och
$g^\prime$ samt andraderivatan $f^{\prime\prime}$. Då ger det en helt
annan betydelse att skriva 
\begin{quote}
--Använd andraderivatan för att... (dvs $f^{\prime\prime}$)...
\end{quote}
än att skriva
\begin{quote}
--Använd andra derivatan för att... (dvs $g^\prime$)...
\end{quote}
Konsekvenserna kan bli ödestigra om en teknisk instruktion innehåller
denna typ av fel. 

Särskrivningar är förrädiska för ibland fungerar de. Man kan skriva
''Jag läser på data teknik programmet'' och bli förstådd trots att det
ska vara ''Jag läser på datateknikprogrammet''. Språket förändras
också mot fler särskrivningar, vilket nog beror på sammanblandning med
hur främst engelskan fungerar, men vissa ord (som exemplet med
sjuksystern ovan) blir bara helt fel om de skrivs isär.  

Särskrivningar finns f ö i \emph{skriven} svenska, inte talad. Vill
man rensa bort särskrivningar så behöver man bara \textbf{läsa texten
  högt} och vara övertydlig med pauserna mellan orden (ta lika lång
paus mellan alla ord). Om det fortfarande låter bra då man läser en
gång till och tar ännu längre paus mellan orden så har man inga
särskrivningar! 

\subsection{Benämning av dokumentdelar}

Det här är en liten sak men det är lätt att råka kalla avsnitt för
''sektioner'' vilket är fel. Det heter \emph{avsnitt} eller
\emph{kapitel} på svenska. Detta misstag beror förstås  på att
kommandot heter \texttt{section}. 

En annan viktig skillnad mellan svenskan och engelskan dyker upp när
man refererar till en numrerad del inne i en mening (inte i början av
meningen). På svenska skriver man t ex  
\begin{quote}
  \ldots som följer av ekvation~2.
\end{quote}
medan det på engelska skulle bli
\begin{quote}
  \ldots that follows from Equation~2.
\end{quote}
Alltså, observera  att på engelska skrivs beståndsdelarnas namn med
\textit{stor bokstav} (t ex \emph{Equation~2}, \emph{Section~1.4} och
\emph{Chapter~3}) men att de skrivs med liten bokstav på svenska
(\emph{ekvation~1}, \emph{avsnitt~1.4} och \emph{kapitel~3}). På
engelska använder man stor bokstav även i förkortningar. Istället för
\emph{Equation~2} skriver man \emph{Eq.~2} och refererar man till
flera ekvationer skriver man \emph{Equations~2 and 3} vilket förkortas
\emph{Eqs.~2 and 3}.  

Det finns, i vissa specifika fall, undantag till regeln om att använda
stor bokstav. Om du inte fått reda på annat så använd stor bokstav. 

\subsection{Fasta mellanslag och ''klister''} \label{sect:samman}

Mellan en beståndsdel och nummer ska man skriva ett fast
mellanslag. Detta beror på att de ska sitta ihop och följa efter
varandra utan radbrytning. \LaTeX-koden för att skriva 
\begin{center}
  ...avsnitt~\ref{sec:kodstruktur}...
\end{center}
är t ex 
\verb| ...avsnitt~\ref{sec:kodstruktur}...|, där den lilla ''vågen'' i
mitten är ett fast mellanslag, ett mellanrum, som fungerar som klister
mellan det som det står mellan. Resultatet är att allt behandlas som
ett ord och hålls samman av \LaTeX. 

\subsection{Storlek}

Detta är betydligt viktigare än man kan tro, och dessvärre något som
kan ta lång tid att få till:  
\begin{quote}
  Hela det färdiga dokumentet ska ge ett intryck av \emph{harmoni},
  och en känsla av \emph{lycka} och \emph{väl\-befinnande} hos
  läsaren.  
\end{quote}
Både sättet som sakinnehållet presenteras på -- att detta är klart och
tydligt -- som typografin och den grafiska designen på bilder påverkar
harmonin. Du skriver naturligtvis för att någon ska läsa, förstå och
gilla det du skrivit.  

Harmoni kan upplevas men är svår att beskriva i tekniska
termer. Storlek spelar roll, framförallt relativ storlek. Samma sorts
text ska genomgående se likadan ut. Löptexten i början av dokumentet
ska  ha samma utseende som den i slutet och bilder ska ha liknande
dimensioner. Att använda \LaTeX\ hjälper mycket till att skapa den
eftertraktade harminin, för dokumenten blir både ''snygga'' och
''lättbegripliga'', men harminin uppstår tyvärr inte per
automatik. Råden i denna skrift leder dock i regel till ökad harmoni
så ta till dig dem. Att slarva förstör harmoni. Visa omsorg om
läsaren.   

\section{Matematiska variabler  i vanlig text}

Om man skriver variabler i löptext ska man omge dem med dollartecken
(eller möjligen använda kommandot \verb|\ensuremath|).  Detta märker
ut matematisk text och skiljer den från vanlig löptext. 
Under inga omständigheter ska textkommandon som \texttt{textsl} och
\texttt{emph} användas för matematik eftersom det då fortfarande är
text och kommer att typsättas som text!  

Notera skillnaden mellan x och $x$, samt i och $i$. Fraserna ''...håll
i konstant...'' (håll tag i nåt hela tiden) och ''...håll $i$
konstant...'' (ändra inte variablen $i$) har helt olika betydelser.  


\section{Multiplikation}

Många skriver ut ett multiplikationstecken i produkter men så gör man
i tryckt matematisk text endast där man annars inte skulle kunna
skilja på faktorerna.  

Produkten mellan $x$ of $y$ skrivs $xy$. Den skrivs varken $x\cdot y$,
$x\times y$ eller $x*y$.  Matematiska variabler består av ett enda
tecken så $xy$ kan inte vara en variabel utan måste vara en
produkt. En situation där man behöver ett multiplikationstecken är
produkter av tal. Då används oftast en enkel punkt, $\cdot$, som
skapas med \verb|\cdot|. T ex så skrivs produkten av $12$ och $34$ som
\verb| $12\cdot 34$| vilket blir $12\cdot 34$. Utelämnar man punkten
och skriver \verb| $12 34$| får man istället  $12 34$ som är helt
missvisande. 

\subsection{Obrukbara alternativ: * och $\times$}

Två tecken som också brukar användas för ''multiplikation'' är  $*$
(en asterisk) och $\times$ (\verb|\times|). Jag tror, utan att veta
säkert, att användningen av asterisk ($*$) för att beteckna
multiplikation kommer från något programmeringsspråk. Inom matematik
betcknar dock $f * g$ något som kallas \emph{faltningen} mellan
funktionerna $f$ och $g$. Faltningar dyker upp i signalbehandling och
är inte multiplikationer.  

Tecknet $\times$ betecknar \emph{Kartesisk produkt} (eller
\emph{kryssprodukt}). För två mängder $A$ och $B$ är $A\times B$
mängden av alla talpar $(a,b)$ där $a$ tillhör $A$ och $b$ tillhör $B$
(eller matematiskt: $A\times B = \{(a,b)|(a\in A)\cap(b\in
B)\}$). Detta är inte heller multiplikation så som vi menar.  

\section{Mellanrum i matematisk text}

I matematiskt text, eller formler, spelar mellanrum oftast ingen 
roll. \verb|123|, \verb|1 23|, och \verb|1 2 3| blir i formler 
$123$, $1 23$, och $1 2 3$. 

Det finns kommandon för att få in mellanrum i formler, men sådana
behövs mycket sällan. Om du vill ha vanlig text i formlerna så använd
kommandot \verb|\text|.  

\section{\texttt{align}}

Omgivningen \texttt{align} används för flera rader med ekvationer som
hör ihop. Den typiska användningen är sekvenser med likheter eller
olikheter. Varje rad har tre delar: Ett vänsterled, en binär relation
(som t ex $=$, $<$, $\leq$, $>$ och $\geq$) och ett högerled. Dessa
delar typsätts som matematik och  kan ha godtyckligt matematiskt
innehåll. Koden 
\begin{verbatim}
\begin{align}
  f(x) &= 2x + 3(x + 2 + 4x) \\
       &= 2x + 3x + 6 + 12x \\
       &= 17x + 6.
\end{align}
\end{verbatim}
ger t ex upphov till 
\begin{align}
  f(x) &= 2x + 3(x + 2 + 4x) \\
       &= 2x + 3x + 6 + 12x \\
       &= 17x + 6.
\end{align}
Vill man inte ha ekvationsnummer på någon rad lägger man in kommandot
\verb|\nonumber| just innan de avslutande dubbla
bakåtsnedstrecken. Skriver man 
\begin{verbatim}
\begin{align}
  f(x) &= 2x + 3(x + 2 + 4x) \nonumber \\
       &= 2x + 3x + 6 + 12x \nonumber \\
       &= 17x + 6.
\end{align}
\end{verbatim}
får man 
\begin{align}
  f(x) &= 2x + 3(x + 2 + 4x) \nonumber \\
       &= 2x + 3x + 6 + 12x \nonumber \\
       &= 17x + 6.
\end{align}
utan nummer på de två första raderna. Vill man helt slopa nummer
använder man omgivningen \verb|align*|. 

\subsection{Enskilda ekvationer} 

Till enskilda ekvationer använder man omgivningen \texttt{equation} om
man vill ha ekvationsnummer, eller annars någon av de ekvivalenta
omgivningarna \verb|displaymath| och \verb|equation*|.  Skriver man 
\begin{verbatim}
  \begin{equation}
    f(x,y) = xy + \sin x
  \end{equation}
\end{verbatim}
får man 
\begin{equation}
    f(x,y) = xy + \sin x
\end{equation}
med ekvationsnummer medan såväl 
\begin{verbatim}
  \begin{equation*}
    f(x,y) = xy + \sin x  
  \end{equation*}
\end{verbatim}
som 
\begin{verbatim}
  \begin{displaymath}
    f(x,y) = xy + \sin x    
  \end{displaymath}
\end{verbatim}
ger
  \begin{displaymath}
    f(x,y) = xy + \sin x    
  \end{displaymath}
utan nummer ute till höger. Man kan också typsätta matematik som
vanlig text genom att omge den matematiska delen med
dollartecken~(\verb|$|). Koden  
\begin{verbatim}
... since $x^2+2x+4=(x+2)^2$, we get...
\end{verbatim}
ger
\begin{center}
  ... since $x^2+2x+4=(x+2)^2$, we get...
\end{center}
Floran av omgivningar och kommandon för typsättning av matematik är
stor men ovanstående räcker långt.  

Vid bedömningen av prov 2 betraktas en alltför vidlyftig användning av
\texttt{align} som ett svaghetstecken. Typsättningen av enskilda
ekvationer och formler ska göras med \verb|equation|, \verb|equation*| 
och \verb|displaymath|. 


\section{Kodstruktur} \label{sec:kodstruktur}

Skriver man ett manus -- och ett \LaTeX-dokument är ett manus -- som
också ska kunna läsas av människor måste man strukturera det så att
det blir lättförståeligt. Med god struktur menas att saker som hör
ihop presenteras tillsammans och att det klart framgår vad som är
beståndsdel och vad som är helhet.  

Som exempel kan vi ta listan 
\begin{enumerate}
  \item En mening.
  \item Mening 2 på nivå 1.
    \begin{enumerate}
      \item En mening på nivå 2. 
      \item Ännu en mening på samma nivå.
    \end{enumerate}
  \item En avslutande mening.
\end{enumerate}
Denna kan skrivas
\begin{verbatim}
\begin{enumerate}\item En mening.\item Mening 2 på 
nivå 1. \begin{enumerate}\item En mening på nivå 
2.\item Ännu en mening på samma nivå.\end{enumerate}\item 
En avslutande mening.\end{enumerate}
\end{verbatim}
vilket är ett exempel på \textbf{dålig struktur}. Det är mycket svårt
att bena ut vad detta är för lista eller ens inse hur många element
listan har. 

Som författare ska man visa omsorg om läsaren och anstränga sig så att
det man skriver är lätt att förstå och därmed blir läst. Ett av många
möjliga exempel på \textbf{god struktur} är  
\begin{verbatim}
\begin{enumerate}
  \item En mening.
  \item Mening 2 på nivå 1.
        \begin{enumerate}
          \item En mening på nivå 2. 
          \item Ännu en mening på samma nivå.
        \end{enumerate}
  \item En avslutande mening.
\end{enumerate}
\end{verbatim}
Detta går att förstå även om man bara kan grunderna i \LaTeX, ja
kanske till och med om man ser \LaTeX-kod för första gången. Varje
listelement börjar på en ny rad som inleds med nyckelordet
\verb|\item|. Radernas \textit{indentering} antyder vad som är ingår i
vad. Vi ser t ex omedelbart att listan har 3 punkter. Att punkt 2 i
listan i sin tur består av en lista (som sägs ligga på nivå 2) syns
också tydligt. Skulle dessutom den listans punkter innehålla
ytterligare listor (eller annat) kan även det tydliggöras genom
indentering på motsvarande sätt. Samma behov av strukturering finns
också då man programmerar datorer, men då använder man förstås andra
språk än \LaTeX.  

När en lärare bedömmer ett manus eller en instruktion du skrivit
tittar denne inte bara på sakinnehållet utan det är också viktigt att
du ger god struktur. Helt korrekta men mycket dåligt strukturerade
manus (datorprogram, \LaTeX-kod, HTML-kod mm) kan mycket väl bli
underkända.  

\section{Listnumrering}

Omgivningen \verb|enumerate| används för numrerade listor, t ex 
\begin{enumerate}
  \item Äpplen
  \item Mjölk
  \item Bröd
  \item Frukt
\end{enumerate}
Om man importerar paketet \texttt{enumerate} med 
\begin{center}
  \verb|\usepackage{enumerate}|
\end{center}
går det sen att ändra det sätt som numreringen sker på. Skriver man t
ex  
\begin{verbatim}
\begin{enumerate}[a)]
  \item Äpplen
  \item Mjölk
  \item Bröd
  \item Frukt
\end{enumerate}
\end{verbatim}
får man bokstavsnumrering,
\begin{enumerate}[a)]
  \item Äpplen
  \item Mjölk
  \item Bröd
  \item Frukt
\end{enumerate}
Man kan också blanda och ha listor i listor:
\begin{enumerate}
  \item En sak. 
  \item
    \begin{enumerate}[I)]
      \item Rom byggdes ...
      \item ... inte på en dag.
    \end{enumerate}
    \item 
      \begin{enumerate}[A:]
        \item Italien är...
        \item ... en stat...
      \end{enumerate}
    \item En sak till.
    \item Och en till. 
\end{enumerate}
Allt som numreras kan ändras i \LaTeX, även rubriknumrering och
ekvationsnummer, men man behöver sällan göra det.  

Att jag tar upp detta beror på att Skolverkets deluppgifter är
numrerade med små bokstäver medan \LaTeX\ numrerar med arabiska
siffror om man inte säger annat. Vill man t ex lista deluppgifterna på
samma sätt som Skolverket så måste man alltså göra som ovan.  

Vid bedömningen fästs inte stor vikt vid hur numreringen görs bara den
görs med en lista då det ur texten framgår att det ska vara en lista
(att använda listor är annars inget krav).  


\subsection{Automatisk numrering}

Använd den automatiska numrering som \LaTeX\ erbjuder så mycket du
kan. Att t ex uttryckligen numrera genom att själv skriva a), b),
c),\ldots i texten är lika dåligt som att själv skriva in
ekvationsnummer och avsnittsnummer. Börjar man möblera om dokumentet
måste man ändra numreringen manuellt, vilket kräver onödigt arbete och
(lätt) riskerar att blir fel. Hårdkoda inte in konstanter i texten
utan använd etiketter (\verb|\label|:s och \verb|\ref|:s).  


\subsection{\texttt{description}} \label{sec:description}

Observera att listelementen i listor med beskrivingar som skapas med
\texttt{description} och \texttt{itemize}-listor \emph{inte} har någon
naturlig numrering! Inkluderar man \verb|\label|:s i sådana får man
inte vad man i regel hade tänkt sig. Koden 
\begin{verbatim}
...
\begin{itemize}
   \item Skriv tydligt. \label{first}
   \item Skriv rätt. \label{second}
   \item Skriv i tid. \label{third}
   \item Skriv ofta. \label{fourth}
\end{itemize}
Tänk i vart fall på punkterna \ref{first}, \ref{second} och \ref{third}.
\end{verbatim}
ger t ex som resultat 

\noindent ''...
\begin{itemize}
   \item Skriv tydligt. \label{first}
   \item Skriv rätt. \label{second}
   \item Skriv i tid. \label{third}
   \item Skriv ofta. \label{fourth}
\end{itemize}
Tänk i vart fall på punkterna \ref{first}, \ref{second} och \ref{third}.''

Anledningen till att alla är lika är att \texttt{itemize} inte
introducerar någon ny numrering. Därför blir alla etiketter istället
kopplade till närmast föregående numrering som råkar vara
avsnittsnumreringen(!) Eftersom den är \ref{sec:description} får även
etiketterna i listan samma värde. På samma sätt blir det om man
använder \texttt{description}. Att etiketterna fungerar i
\texttt{enumerate}-listor beror på att \texttt{enumerate} startar en
ny numrering\footnote{För text omedelbart efter listan gäller återigen
den numrering som gällde innan. Detta gör att man också kan ha
numrerade listor i numrerade listor och ändå få separat numrering
inom respektive lista.}.   

\section{Markera att punkt i mening inte avslutar meningen}

I löptext gäller generellt att en punkt avslutar en mening. Om man har
en punkt \emph{inne i en mening} måste det mellanrum som följer efter
denna punkt markeras speciellt med ett \emph{bakåtsnedstreck}
(\emph{back-slash}, \verb|\|). Exempel: Texten
\begin{center}
   I Sverige har vi t.\ ex.\ en huvudstad och fyra årstider.  
\end{center}
skrivs
\begin{verbatim}
I Sverige har vi t.\ ex.\ en huvudstad och fyra årstider.
\end{verbatim}
med bakåtsnedstreck efter varje punkt förutom den
avslutande. Anledningen är att \LaTeX\ lägger till olika mycket
mellanrum beroende på vilken sorts punkt det är. 

Man kan jämföra bakåtsnedstreck med den våg (\verb|~|) som omnämns i
avsnitt~\ref{sect:samman}. Vågen ger även den ett mellanrum men
hindrar dessutom radbrytning. Om, men endast om, man vill både markera
att en punkt inte avslutar en mening \emph{och} att radbrytning inte
får ske, använder man en våg istället för ett bakåtsnedstreck.  

\subsection{Kuriosa} 

Den som läser detta dokuments källkod ser att jag skriver
\begin{center}
 \verb|...numrering som \LaTeX\ erbjuder...|
\end{center}
några rader upp i texten (för att få ''...numrering som \LaTeX\ erbjuder...'')
och det beror på att kommandon tar nästa tecken i texten som
argument. Skippar jag det avslutande bakåtsnedstrecket och istället
skriver  
\begin{center}
 \verb|...numrering som \LaTeX erbjuder...|
\end{center}
får jag ''...numrering som \LaTeX\ erbjuder...''), dvs mellanrummet ''äts upp''
av kommandot \verb|\LaTeX|. Om det försvinner mellanrum kan lösningen
ibland bestå i att skjuta in ett bakåtsnedstreck.  
\end{document}
