# Föreläsning 2016-09-06

Dagens föreläsning handlar om LaTeX.

LaTeX tolkar ett manus och kompilerar detta till ett dokument. LaTeX sköter all typsättning. Eftersom man gör fel hela tiden kan det vara en bra idé att använda ett iterativt arbetssätt. På så sätt upptäcker man fel tidigt.

När man skriver LaTeX bör man använda systemet som det är tänkt. Tycker man annorlunda än LaTeX är det antagligen en själv som har fel.

Ett bra IDE för LaTeX är *kile* som finns i LTU:s datorlabb. Alla uppgifter måste redovisas i labbet.

## Kommandon och omgivningar

Kommandon inleds med backslash `\`. Därefter själva kommandot. Det kan antingen hämta in data som behövs för genereringen eller visa upp något specialtecken.

Omgivningar börjar med `\begin{environment-name}` och slutar med `\end{environment-name}` Omgivningar indenteras.

* `includegraphics`
    * finns i paketet `graphicsx` och importerar bilder
* `caption`
    * caption till bilder
* `label`
    * en referens till en `label`
* `ref`
    * en referens inom dokumentet
* `centering`
	* centrerar ett objekt
* `setlength`
    * sätter avstånd mellan ett objekt och dess ram

Man kan centrera med hjälp av kommandot 

Till ett kommando har man `{}` som berättar vad kommandot påverkar. `[]` är mer tillval.

## Paket

* `inputenc`
    * bestämmer teckenkodning
* `babel`
    * justerar typsättningen efter ett specifikt språk
* `amsmath`
    * innehåller hjälpmedel för att generera matematikformler

## Tabeller

Tabeller görs med `&` mellan kolumnerna och \hline mellan raderna.

    \begin{table}[tbh]
	\begin{tabular}[c]{|I|c|c|}
        \hline
        1 & 2 & 3 \\
        \hline
        3 & 4 & 5 \\
        \hline
    end{tabular}
    end{table}
