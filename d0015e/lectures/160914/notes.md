# Föreläsning 2016-09-14

Datorn och informationssamhället

## Datorn

Datorn kom i olika generationer. 

* I begynnelsen var elektronrören
* 1947 uppfanns transistorn
* 1959 började man göra integrerade kretsar
* Under 70-talet började man kunna göra hemmabyggen
* Unde 80-talet blev datorer allmänt tillgängliga

## Enkla beräkningshjälpmedel

* Kulramar
* [*Napier bones*](https://en.wikipedia.org/wiki/Napier_bones)
* [*The Pascaline*](https://en.wikipedia.org/wiki/Pascaline)
    * Kunde addera och subtrahera
* [*The Leibniz wheel*](https://en.wikipedia.org/wiki/Leibniz_wheel)
    * Liknande konstruktioner användes i räknemaskiner i 300 år, fram till 1960-talet
* Räknestickan
    * Uppfanns under 1622 av William Oughtred
    * Med hjälp av två logaritmiska skalor kunde man snabbt genomföra stora multiplikationer
* Miniräknaren

1830 skissade Charles Babbage (1792-1871) en mekanisk beräkningsmaskin. [*Ada Lovelace*](https://en.wikipedia.org/wiki/Ada_lovelace) konstruerade algoritmer för maskinen. I Sverige konstruerades en räknemaskin med printer 1843. Den såldes i tre exemplar.

## Hålkort

Kort med utstansade hål användes för att lagra vävmönster från 1725. Med tiden började man lagra annan data. När man kunde styra maskiner med hålkorten. Det ökade produktionen och gjorde många vävare arbetslösa. Detta ökade klyftorna mellan rik och fattig. (Ordet *sabotage* kommer från franskans *sabot* vilket betyder träsko, då vävarna gick in och slog sönder hålkortsmaskiner med träskor)

Herman Hollerith (1860-1929) effektiviserade 1890 års befolkningsräkning med hjälp av egendesignade hålkort och en elektronisk avläsningsapparat. Han la senare grunden för företaget IBM.

![Lager av hålkort](halkort.png)

## Elektromekaniska räknare

Konrad Zuse (1920-1955) konstruerade världens första elektromekaniska räknare 1937 under nazi-regimen. Den hette Z1 och den följdes av Z2, Z3, Z4, Z5. 1942 drogs hans finansiering in på grund av att det gick dåligt i kriget.

Harvard och IBM byggde under kriget elektromekaniska räknare för ballistiska beräkningar. Den var 16 meter lång.

I Sverige byggde man *Binär Automatisk Relä Kalkulator*. Den byggdes av telefonreläer.

## Elektriska datorer

Under kriget använde tyskarna kryptona *Lorentz* och *Enigma*. Alan Turing byggde maskinen *Colossus* på Bletchley Park som kunde knäcka många av koderna. Arbetet skedde i största hemlighet och förkortade antagligen kriget med flera år.

*ENIAC* betraktas som världens första elektroniska dator. ENIAC står för *Electronic Numerical Integrator and Calculator*. Den styrdes med elektronrör. Tyvärr gick dessa ofta sönder och behövdes bytas ut, vilket inte alltid är så enkelt när man har flera tusen.

John von Neumann (1903-1957) förstod att program också kan beskrivas som data. Du kan alltså lagra programmen på hålkort istället för att hårdkoda datorerna. *EDVAC* kunde 1950 för första gången läsa in, lagra, utföra och byta ut sina program med hjälp av ett minne.

1955 ett tag bytte man ut elektronrören mot transistorer. Därefter kom man på att man kunde tillverka transistorer fotografiskt och kunde göra integrerade kretsar. Till slut blev transistorerna så små att man kunde stoppa in en hel dator i en integrerad krets. Man hade fått en mikroprocessor.

## Transistorn

En transistor fungerar som en strömbrytare som kan slås på och av med ström. I datorer kodar man olika logiska funktioner med hjälp av olika spänningsnivåer. Med strömbrytare konstruerar man logiska funktioner som OR, NOT, NAND och så vidare. Av dessa funktioner kan man därefter konstruera mer komplicerade funktioner.

Exempel på en logisk funktion som kan addera:

![Logisk funktion](logik.png)

## 1968

1968 sker mycket runt om i världen. Bland anna påbörjas projektet ARPANET som senare utvecklas till Internet. Språket *C* släpps också. Operativsystemet *UNIX* släpps. 1972 har det skrivits om till C. *2001: A Space Odysseus* har premiär.

## 70-talet

Databranschen uppstår under 70-talet. Många äldre företag satsar stort på den nya tekniken. Det startas även många nya företag. Man forskar mycket kring kryptering och lägger grunden för asymmetrisk kryptering.

TCP/IP skrivs och lägger grunden för det moderna Internet. Sverige storsatsar på datorer.

Produkter börjar produceras med mikroprocessorer för att kunna göra mer flexibla produkter.
## 80-talet

Internet Engineering Task Force startar. GNU likaså. Det släpps många hemdatorer och datorgrafik börjar bli vanligare. "We reject: kings, president and voting. We believe in: rough consensus and running code."

Datorn blir allmänt tillgänglig för människor. I Sverige satsar man på datateknikutbildningar på tekniska utbildningar. Dessa utbildningar fokuserar mer på diskret matematik än traditionella ingenjörsutbildningar. Detta på grund av att datorn är diskret till sin natur.

## Digital revolution

Under 70- och 80-talen byts mycket som varit analogt ut mot digitalt. Mycket är billigare och enklare att göra. Digitalpianon, digitalkameror, datorspelskonsoler och så vidare släpps. Detta påverkar också musikutvecklingen.

Digitala lagringsmedia utvecklas. Floppies blir vanliga och magnetband används i stor skala.

## Informationssamhället

Från 1800-talet har behovet att överblicka information vuxit. Mycket i samhället idag handlar om att flytta och behandla information.

Vi har till och med börjat betrakta oss själva som information, med upptäckter inom gentekniken. Det öppnar för att vi ska kunna ändra på vår egen art. Gränserna mellan levande och maskin suddas ut.

Under 1800- och 1900-talets järnvägar och bilvägar ledde till revolutionerande förändringar. På samma sätt verkar dagens nätverk att leda till stora förändringar.

## 90-talet

Linux släpps. Webben släpps och näthandeln blir till. Mobiltelefonen anländer.

Plötsligt vet man inte var människor är. Man kan kommunicera med människor var de än är. Det förändrar världsuppfattningen.

## Nu

Hälften av världens befolkning använder Internet. Nätet blir till *Molnet*. Sociala nätverk växer fram osv. Icke-personer kopplas upp mot Internet. Folk börjar prata om *Internet of Things*.

* Vad har vi rätt till på nätet?
    * Anonymitet?
    * Privatliv?
* Vem bestämmer på nätet?
    * Politiker?
    * Företag?
    * Anarki?

Vad är *original* och vad är *kopia*? Affärsmodeller förändras.

## Integritets- och fildelningsdebatten

Varför är debatten i Sverige så livlig?

Sverige har en gammal tradition av att vi inte har någon förhandscensur (med undantag av filmcensuren). Vi har också meddelarfrihet och liknande. Vi har inte haft krig på 200 år. Det har gjort att vi känner att vi inte behöver en stat som kontrollerar.

Vi har en väldigt hög teknisk nivå. Vi klarade oss undan andra världskriget och har stora naturtillgångar. Sverige är ett av världens mest individualistiska samhällen. Allt detta medverkar till en syn att individen ska få göra lite som de vill.

Vad menas med liv? Barn uppfattar datorer som levande och med medvetande.

I och med nätet kan man plötsligt ha flera *persona*? Tidigare hade du en plats i samhället som du hade hela livet, men i och med nätet kan man vara lite vem man vill när man vill.
