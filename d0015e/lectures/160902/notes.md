# Föreläsning 2016-09-02

Idag kommer handla om hur man vanligtvis styr datorer. Unix är namnet på ett operativsystem. Unix är väldigt spritt. Det innebär att om man kan hantera Unix kan man hantera de flesta datorer, på plats över Internet. Det är bland annat Unix möjlighet till fjärrinloggning som man kan använda för att ta över någon annans dator.

# Starta datorn

När man startar en dator bootar den. Allt strömsätts. CPU tar reda på vad den har för resurser. Detta är i firmware, innan operativsystemet. Den här processen kan man inte påverka. När firmware är klar läser den in OS från HDD till RAM. Det gör man genom att man i förväg har placerat OS på en bestämd plats på HDD.

En *loader* är ett program som laddar över program när det startar. Därefter tar en *schemaläggare* över och ser till att det verkar som att alla program körs samtidigt. Det körs bara ett program i taget, men man växlar vilket program som körs hela tiden så att det verkar som att de körs manuellt. Schemaläggaren körs av en *dispatcher*, som är processen som ser till att programmen faktiskt körs. Allt det här körs på, och av, CPU.

# Kör datorn

Innan användning måste man logga in. Kan man inte logga in får man inte köra program. Vid inloggning startas en skalprocess. Dessa tillåter input i textform och kan köra program.

Allt som körs i datorn är processer. Vi kan starta användarprocesser, men det finns mer priviligerade systemprocesser som datorn och sysadmin kan köra. Ju fler kommandon man kör desto mer processer körs det.

När en process startar får den en mängd minne reserverat. Detta utrymme får bara användas av den aktuella processen. Om minnet är dåligt organiserat försöker datorn att rätta till det så fort den får tid över. I varje process minne står det programmets *state*. Det handlar om vad som exekveras just nu, programmets data samt programmets kod.

# Filsystem

Ett Unixsystem är organiserat i ett filsystem. Detta kan symboliseras som ett upp och nervänt träd. Genom att följa var i trädet en fil eller katalog är placerad kan man få ut en unik adress.

Filsystemet är som en karta. Det ligger på ett litet område av HDD och visar precis var den faktiska datan för varje fil finns. Filsystemet är alltså någon slags metastruktur, filerna ligger egentligen inte i själva mapparna.

# Datorns system

Man kan se datorn som en lök. Allra innerst finns den fysiska datorn. Utanpå den finns kärnan, som är den som sköter direktkommunikationen med hårdvaran. Den märks i regel inte av användaren. Utanpå kärnan finns ett skal, som är det som användaren kommunicerar med. I detta kör användaren sina program.

* Hårdvara
    * Kärnan
	    * Skal
		    * Program

# Kommandon

Det finns många, många olika kommandon. Här följer några exempel.

* echo <text>
    * skriv ut <text>
* date
	* aktuell tid och datum
* man <kommando>
	* visa manual för <kommando>

Små textkommandon är bra att kunna använda, särskilt om man bara har bara tillgång till en textbaserad fjärrinloggning.
