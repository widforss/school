# Föreläsning 2016-0922

## Effektiva algoritmer

En algoritm är en tanke om hur man löser ett problem. Programmerare tänker i termer av algoritmer.

Översikt:

* Algoritm
    * Vad är det?
* Effektiva algoritmer
    * En telefonlista
* Hur hittar man namnet?
    * I telefonlistan
    * Två olika metoder
* Slutsatser
* Analys
    * Hur fort går det?
* Sortering
    * Ett exempel
* Forskning
* Hemuppgift

### Exempel 1

När man använder algoritmer vill man att de ska vara så effektiva som möjligt. Det innebär att man vill ha så få steg som möjligt för att hitta lösningen. Har en människa en telefonlista tittar man kanske lite översiktligt och ser om vederbörande kan hitta personen.

En maskin kanske använder *brute force*, man tittar helt enkelt om personen matchar med en rad, därefter tittar man på nästa rad, och så vidare, tills man hittar en matchning. För att säkerställa att namnet inte finns i listan måste man titta igenom hela listan.

Är listan i alfabetisk ordning kan man använda en annan algoritm. Om man börjar med att titta på mitten av listan ser maskinen snabbt om det eftersökta namnet är över eller under det mittersta namnet. Därefter struntar man i den irrelevanta delen och tittar på mitten av den som namnet kanske finns i.

![Telefonlista](telefon.png)

Den första algoritmen skalar linjärt. Om listan blir dubbelt så stort ökar antalet steg lika mycket. Men den andra algoritmen behöver bara en extra sökning för dubbelt så många namn. Den är alltså ganska effektiv. Antalet jämförelser som krävs för den första algoritmen är i värsta fall lika många som antalet element i listan. Den andra algoritmen kräver `log n + 1` jämförelser där `n` är antalet element i listan, vilket innebär väldigt mycket färre sökningar jämfört med antalet element. **Dock krävs att listan redan är sorterad.**

Motsvarande prestandavinster kan vinnas inom många områden.

### Exempel 2

Anta att vi har en lista som vi vill sortera. Det kan man göra med hjälp av *Mergesort*. Det innebär att man

* delar listan i två delar,
* sorterar varje dellista med Mergesort, och
* sätter ihop de två listorna till en lista.

Notera att algoritmen är rekursiv, vilket kommer att leda till att den i slutändan kommer att jämföra talpar, vilket är trivialt.

En annan sorteringsalgoritmt är *ZelectionSort* (en förenkling av *Selectionsort*. Den innebär att man gång på gång går igenom listan och plockar in det minsta värdet i listan. Det gör man tills man har plockat samtliga element.

Komplexiteten för Mergesort är `n + log n`. För ZelectionSort är komlexiteten `n(n - 1) / 2`. Eftersom den tidigare är logaritmisk och den senare är kvadratisk. Det innebär att Mergesort är mycket snabbare för stora listor.

### Exempel 3

Givet en lista `[-3, 3, -1, -1, 4, -2, 1]`, vilken dellista kommer att resultera i den största summan (`[3, -1, -1, 4]`)?

Den naiva lösningen är ett gå igenom varje möjlig dellista och summera denna. Det är dock inte särskilt effektivt, komplexiteten är i storleksordningen O(n³).

Ett svårare problem är att summera `k` delsummor och få en så stor delsumma som möjligt. Forskare från LTU har hittat *linjära* algoritmer för att lösa detta problem.

## Computational geometry

*Computational geometry* är ett eget forskningsfält som handlar om att hitta bra algoritmer som handlar om geometri. Detta används för datorgrafik, ruttplanering för robotar, inom GIS och CAD.

### Konvexa höljet

En yta är konvex om en linje som förbinder två punkter på ytan inte kan gå utanför ytan.

Problemet är att givet en punktmängd `S` ge det kortaste slutna polygontåg som innehåller `S`. Man kan tänka sig en mängd spikar, där man ska svara på hur kort ett gummiband som innesluter alla spikar kan vara.

#### SlowConvexHull

#### Jarvis March

#### MergeHull
