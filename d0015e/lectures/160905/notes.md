# Föreläsning 2016-09-05

Den datatekniska historien

* Datateknisk förhistoria
    * Kursbokens sidor 1-136
* Historiemomentets upplägg
	* 3 föreläsningar
	* Prov tisdagen den 20 september
* Litteratur
	* Sundin, *Den kupade handen*
	* Hansson *Datoriseringen* ur *Den skapande människan* (utdelas senare)
	* Eventuellt något litet annat

## Teknikens början

Vi tillhör arten *Homo Sapiens*. Något som är speciellt för människan att vi använder verktyg. Det finns andra arter som använder verktyg, men inte på samma nivå som människan. Det första tekniska verktyget var förmodligen något slags stenverktyg.

Tekniken är en kulturell företeelse. Det innebär att det är ett resultat av en social tradition. Tekniken traderas mellan generationerna, det förs inte över genetiskt.

## Historiska samhällsstadier

* Jakt- och födosamlande
	* ~2,5 miljoner år - ~125 000 generationer
* Födoproducerande jordbruk
	* 10 000 år - ~500 generationer
* Industriellt samhälle - ~10 generationer

Det diskuteras om vi nu är på väg in i nästa fas: *informationssamhälle*

## Språket

Språket är en central "uppfinning" för att tradera kunskap mellan individer och generationer. Språket skiljer oss från djuren och möjliggör tänkande med symboler och begrepp. Inom datatekniken använder vi ofta artificiella språk för att kommunicera med datorer och för att få datorer att kommunicera med varandra.

Skriften är en teknik för att förmedla information med hjälp av symboler. Med hjälp av skrift blir det möjligt att ge order utan att vara närvarande. Det blir möjligt att organisera stora nationer. Man kan också _lagra_informationen_ så att den kan föras vidare. Det blir också enklare att kritiskt granska texten.

Det enklaste skriftspråket är *bildskrift*. I ett sådant skriftspråk är varje bild ett begrepp. Efter ett tag får man symboler för abstrakta begrepp. Vad ska man välja för symbol för *kall*, *snäll*, *abstrakt*, *Gud* osv?

Bildspråk är svåra att lära sig och få har råd att investera tid för att lära sig dessa språk. Det gör att det skapas en klass av skriftlärda. Dessa får stor makt och det bildas en klyfta mellan teoretiker och praktiker. De som behärskar informationsteknologin i sin tid har alltid haft en nyckelställning - och makt - i samhället. Se [Edward Snowden](https://en.wikipedia.org/wiki/Edward_Snowden).

Efter ett tag förlorar tecknen sin individuella mening och behåller enbart sitt ljud. Man inför också konsonanter, tecken vars ljud man inte kan uttala ensamt utan en vokal. Kombinerar man vokaler och konsonanter får man hela ord. Det är först de hela orden som har en mening.

Detta alfabet är betydligt enklare att lära sig eftersom det har mycket färre tecken. Det gör att fler kan lära sig och att de skriftlärdas makt späds ut.

Exempel på språk:

![Skriftsprak](sprak.png)

## Skrivmaterial

Ursprungligen skrev man på träbitar eller hopflätade blad, *papyrus*. Dessa material var inte särskilt bra material. I Romarriket kom man på att man kunde göra en *codex*. Då vek man ihop olika blad till en "bok" som gick att söka i mycket snabbare än en bokrulle. Med bättre material kunde man bevara informationen längre. Man bytte ut *papyrus* mot skinnprodukten *pergament*.

## Definerande teknik

Människan förklarar alltid sin omvärld och universum i termer av aktuell teknik.

Världen är gjord som:

* Drejning
* Spinning
* Svarvning
* Mekaniska urverk
* Ångmaskinen
* Datorn

Teknikutvecklingen går alltså hand i hand med vår förståelse för omvärlden.

### Urverket

Klosterlivet på 1200-talet krävde tidmätare. Munkarna hade kunskaper och behovet att tillverka ur. Urverket anses vara den moderna industriålderns nyckelmaskin. Den var viktigare än t.ex. ångmaskinen.

Dag och natt var ursprungligen jämnt indelad i 12 timmar vardera. En timmes längd varierade alltså över dygnet och året. Behovet av regelbundenheten och uppfinningen av urverket gjorde dygnets alla timmar lika långa. Tiden blev abstrakt som löper oberoende av naturen.

Urverket förändrade vår syn på världen. Gud blev "den store urmakaren", men efter han hade startat urverket Världen gick det av sig självt. Det gjorde att det kunde vara okej att hitta olika *naturlagar* som förklarade hur Guds skapelse fungerade.

Detta ledde i sin tur till att kyrkans världsbild fick konkurrens, där den geocentriska världsbilden med tiden förkastades.

## Boktryckarkonsten

Konsten att trycka böcker med *rörliga (lösa) typer* uppfinns under 1400-talet. [Gutenberg](https://en.wikipedia.org/wiki/Johannes_Gutenberg) har fått symbolisera boktryckarkonsten med sin bibel. Alternativet till att trycka böckerna är att man skriver de för hand, något som är väldigt dyrt och gör det lätt att introducera fel som blir värre med antalet "generationer" avskrivningar.

När man kan massproducera böcker blir det värt att översätta böcker till andra språk. Det gör att varje hem kan få en bibel. Det leder till att den kristna makten decentraliseras och försvagas.

Det kommer också hantverksböcker. Istället för att bara tjäna pengar på att utföra hantverk kan man nu tjäna pengar på att ge ut instruktioner gällande hantverket. Detta leder till ett behov av juridiska skydd i form av copyright och patent.

## Siffror

Samtidigt inför Fibonacci det arabiska talsystemet. Med det kan man enkelt notera tal på ett sätt som är bekvämt för beräkningar. Detta ledde till en ökad ekonomisk utveckling.

## Papper

Papper är billigare än pergament. Till en bibel behövde man 200-300 får, vilket var väldigt tyg. Genom att använda lump från tygindustrin kom man på att man kunde göra billigare *papper*. När efterfrågan på papper blev för stor för tygindustrin började man istället använda trämassa.
