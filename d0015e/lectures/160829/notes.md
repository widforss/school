# Föreläsning 2016-08-29

## Idag

Dagens föreläsning handlar om kursen och vad den innebär.

Håkan Jonsson är civilingenjör i robotik samt tekn. dr. i datalogi. Det är han som är examinator i kursen.

## Kursplanen

Läs [kursplanen](http://www.ltu.se/edu/course/D00/D0015E/D0015E-Datateknik-och-ingenjorsvetenskap-1.67711?kursView=kursplan) när ni ska gå en kurs. Där står vad man ska kunna när kursen är klar.

Datateknik är det som påverkar mest idag. Datorer är integrerade i nästan alla system idag. Det gör att datatekniken automatiskt blir en väldigt bred utbildning. Kursen ska inte ge verktyg för att använda datorer utan ge en fundamental förståelse för hur datorer fungerar. Vi ska också se vad det finns för problem kopplade till datatekniken. Dessa är ofta kopplade till interaktionen mellan maskin och människa.

Vi ska under kursen förhålla oss vetenskapligt till det vi arbetar med. På universitet söker man sanningen.

## Hur man pluggar i den här kursen

Det finns uppgifter och deadlines under hela läsperioden. Mycket självstudier kräver förhållningssätt, ansvar och självdisciplin. Delta i allt schemalagt. Vissa pass är obligatoriska. För frånvaro från dessa krävs *tvingande skäl*. Om man är frånvarande från ett obligatoriskt pass måste man komplettera.

Kursen har _ingen_avslutande_tentamen_.

På kursens [hemsida](https://sites.google.com/site/d0015eht16/) finns kursens bildspel, sammanfattningar och annat nyttigt. Det finns också ett [kursforum](https://groups.google.com/forum/#!forum/d0015e-ht16-forum) samt [videoföreläsningar](https://test.scalable-learning.com/#/courses/9622/modules/19158/courseware/lectures/46850).

Alla mail och så vidare ska skickas från studentmailen, aldrig privat mail.

[Canvas](https://ltu.instructure.com/) används till inlämningar i den här kursen.

Samtliga studenter får ett utskrivet schema där kurs- och examinationstillfällen antecknas.

## Kursinnehåll

### A. Datorns delar

Plocka isär en dator.

#### Obligatoriska pass

* 31/8 10.15
* 1/9  08.30

### B. Informationssökning och referenshantering.

Lär dig att söka och granska fakta.

#### Obligatoriska pass

31/8 16:30 eller 6/9 13:00

### C. UNIX

Hur funkar operativsystemet UNIX?

#### Deadline

8/9

* Lämna in PDF i Canvas
* Redovisa UNIX

### D. LaTeX

Lär dig språket LaTeX

#### Deadline

13/9

* Lämna in PDF i Canvas
* Redovisa LaTeX

### E. Datahistoria

Om bakgrunden till datatekniken

Litteratur:

* Sundin, Den kupade handen - Människan och tekniken (ISBN 9173310158)
* Hansson, Datoriseringen ur Den skapande människan

#### Prov

20/9

### F. Webben och HTML

Vad är webben?

#### Deadline

22/9

### G. Pågående forskningsprojekt

Grupparbete där vi ska intervjua forskare om deras forskningsprojekt.

#### Deadline

26/9

* Intervjuplanering

3-4/10

* Redovisning

3/10

* Lämna in presentation

3-4/10

* Genomför presentation

### H. Inbyggda system

Programmera Arduino

#### Deadline

6/10

* Redovisa i labbet
* Lämna in i Canvas

### I. Datatekniknära forskning

Föreläsning av forskare om specifika ämnen

#### Deadline

12/10

* Skriv kort rapport och lämna in i Canvas

### K. Framtidens datateknik

Framtidsstudie i grupp

#### Deadline

18/10

* Lämna in presentation

18-19/10

* Genomför presentation

### L. Yrkesrollen

Vad gör ingenjörer på jobbet?

#### Deadline

21/10

* Lämna in reflektion av studiebesöket.
