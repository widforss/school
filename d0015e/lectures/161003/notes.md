# Föreläsning 2016-10-03

Dagens föreläsning handlar återigen om algoritmer.

## Är P=NP?

*Kan varje beräkningsproblem vars lösning kan verifieras snabbt av en dator även lösas snabbt av en dator?* Alla snabbt lösbara problem bildar mängden `P`. Alla problem vars lösningar snabbt kan kollas utgör mängden `NP`. Frågan är alltså, **om det är enkelt att kolla en lösning, är det då enkelt att hitta en lösning?** De allra flesta tror att så inte är fallet.

Det finns idag problem som inte kan lösas snabbt trots att deras lösningar kan verifieras snabbt. Många viktiga problem verkar bete sig såhär.

### Bakgrunden till problemet

*Kurt Gödel* skrev 1956 problemet i ett brev till *John von Neumann* i ett brev. Dagens problemformulering **P=NP** är från 1971, formulerat av *Richard Karp, Stephen Cook* och *Leonid Levin*. Det är det viktigaste problemet inom modern datavetenskap.

### Snabb lösning?

En snabb lösning menas att en lösning tar ett *polynomiellt* antal steg. Det vill säga att lösningen tar `Cn^k` antal steg där `C` och `k` är konstanter.

Om lösningen tar längre tid än detta är lösningen långsam och problemet är svårt. Den snabbaste ökningen än *exponentiellt*, problem av typen `Ck^n`.

### Exempel

Alla nedanstående problem är *svåra* att lösa.

#### Subsetsum

> Givet en mängd med n tal, finns det en icke-tom delmängd vars summa är noll?

En naiv lösning är att testa alla olika delmängder. Men antalet delmängder växer väldigt snabbt jämfört med mängden, den växer *exponentiellt*. Tyvärr verkar detta vara den snabbaste lösningen just nu. Problemet verkar alltså vara just **svårt**.

#### Handelsresandeproblemet

> En handelsresande vill besöka 100 städer genom att köra från sitt hem och sedan tillbaka. Finns det någon ordning som den handelsresande kan köra runt till alla städerna utan att köra längre än absolut nödvändigt?

Också detta är ett svårt problem. Den snabbaste lösningen handlar i stort sett om att testa alla möjliga vägar.

#### Schemaläggning av tentor

> Studenter vid ett inte närmare namngivet nordligt beläget tekniskt universitet läser alla varje läsperiod en eller flera kurser. Varje kurs avslutas med en obligatorisk tenta som tar 4 timmar att skriva.
>
> För att hindra fusk måste alla som tar tentan i en kurs ta den samtidigt. Om en student läser flera kurser måste tentorna i de kurserna ges olika tider.
>
> Givet en examinationsperiod med 5 dagar, finns det något sätt att schemalägga tentorna på så sätt att alla studenter kan tentera sina kurser.

#### Packning av filer

> En student har 100 filmer och vill spara dessa på USB-minnen. Filmerna har väldigt varierande storlek. Ingen fil får delas. Varje minne rymmer 32 GiB.
>
> Räcker det med 10 minnen?

#### Tentavaktningsproblemet

> Man vill införa kameraövervakade tentor. Tentorna ges överallt på campus, inte bara i klassrum och tentasalar. För att ingen ska kunna fuska så måste varje punkt på campus vara övervakad.
>
> Räcker det med 100 övervakningskameror?

#### Vänskapsrelationer

> Antap att man har en fullständig lista på vilka studenter som är vänner. Finns det en grupp med 40 studenter där alla känner alla?

#### Inbrottstjuvens dilemma

> En tjuv hittar vid ett inbrott en stor mängd dyrbarheter. Varje dyrbarhet är odelbar samt har viss volym och ett visst värde.
>
> Hans säck rymmer 30 liter.
>
> Ryms det dyrbarheter för 1 000 000 kr i säcken?

#### En knäckfråga

> I en påse finns 100 st hemgjorda knäck som alla väger lite olika. Går det att dela upp alla knäck i två högar så att högarna väger lika mycket.

#### Logisk satisfierbarhet

> Givet en logisk krets, finns det en tilldelning av värden (0 eller 1) till variablerna x1, x2, x3, ... sådan att kretsen ger 1 som utdata?
>
> Dvs, går det att få ut ett positivt värde ur kretsen?

Problemet kallas **SAT**, som i *SATisfiability*. Man kan tänka sig att en lösning på problemet skulle kunna göra det enklare att förenkla logiska kretsar.

### Problem och lösningsmetoder

Viktigt med `P` och `NP` är att `P` ingår i `NP`.

En annan viktig grupp utgörs av de så kallade NP-kompletta problemen. Det är de problem där den enda nuvarande lösningen är *brute-force*. Om man löser ett NP-komplett problem är sannolikheten stor att man har löst alla andra.

![N, P, NP och NP-hard](NP.png)
