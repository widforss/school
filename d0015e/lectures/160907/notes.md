# Föreläsning 2016-09-07

Dagens föreläsning handlar om elektricitet och dess påverkan på samhället. Tiden är *upplysningstiden*

Sidorna i boken är s. 189-273.

## Upplysningstiden

Inom naturvetenskapen har man upptäckt att man kan beskriva världen matematiskt. Newton är en stor symbol för upplysningen då han formulerade flera naturlagar. Detta som en del av världsbilden av världen som ett urverk som nämndes tidigare. Med tiden stämde kyrkans världsbild allt sämre med den nya modellen. Det ledde till kritik mot kyrkans makt.

Man kommer på att man ofta kan låta maskiner utföra enformiga arbeten såsom kardning av ull. Man utvecklar också ångmaskinen. På så sätt fick man en kraftkälla som var oberoende av naturen och vädret.

I Frankrike sker det en revolution 1789 med målen *jämlikhet* och *demokrati*. Dessa tankar sprider sig även till Sverige, men kväses snabbt. *Voltaire* var en framstående tänkare. Han menade att alla människor var begåvade med ett förnuft och hade en rätt att uttrycka sina tankar.

## 1700-talets elektriska upptäckter

Elektricitet har varit känt sedan antiken. Dock har man bara sett det i form av statisk elektricitet och blixtar.

[Luigi Galvani](https://en.wikipedia.org/wiki/Galvani) (1737-1798) upptäckte att döda grodlår kunde förmås att röra sig om de var i kontakt med metallbitar. [Alessandro Volta](https://en.wikipedia.org/wiki/Alessandro_Volta) (1745-1827) skapar en *Voltastapel*, ett batteri med koppar och zink. Galvani gav namn till den [*galvaniska cellen*](https://en.wikipedia.org/wiki/Galvanic_cell) och Volta gav namn till enheten [*Volt*](https://en.wikipedia.org/wiki/).

[Hans Christian Ørsted](https://en.wikipedia.org/wiki/Hans_Christian_%C3%98rsted) (1777-1851) upptäckte 1820 att en elektrisk ström gav upphov till ett magnetfält.  [André-Marie Ampère](https://en.wikipedia.org/wiki/Andr%C3%A9-Marie_Amp%C3%A8re) (1775-1836) försöker samma år att förklara elektromagnetismen matematiskt. Amperé upptäckte också att den elektriska strömmen hade en riktning. Han gav upphov till enheten [*Ampere*](https://en.wikipedia.org/wiki/Ampere). [William Sturgeon](https://en.wikipedia.org/wiki/William_Sturgeon) (1783-1850) upptäcker 1824 att man med en spole kan förstärka magnetfältet.

[Michael Faraday](https://en.wikipedia.org/wiki/Michael_Faraday) (1791-1867) upptäckte att strömmen håller sig på utsidan av en ledning. Det demonstrerade han 1836 genom att konstruera [*Faradays bur*](https://en.wikipedia.org/wiki/Faraday_cage). Samme man kommer lite tidigare, under 1820-talet att man kan få fram en ström genom induktion. Med hjälp av den insikten konstruerade [Hippolyte Pixii](https://en.wikipedia.org/wiki/Hippolyte_Pixii) (1808-1835) den första praktiska generatorn.

## Telegrafen

Under 1830-talet möjliggjorde telegrafen kommunikation över stora avstånd. Den apparaten var helt beroende på elektromagnetens funktion.

![Telegraf](telegraf.png)

Exempel på tidigare fjärrkommunikation:

* Kurirer
* Brevduvor
    * Svenska marinen använde brevduvor till 1890-talet.
* Vårdkasar
* Semaforering
* Optisk telegraf

[Samuel Morse](https://en.wikipedia.org/wiki/Samuel_Morse) konstruerade runt 1840 sitt alfabet [*morsekod*](https://en.wikipedia.org/wiki/Morse_code). Det gjorde det möjligt att skriva ett meddelande genom att bara använda två ställningar, *ström* och *ingen ström*, som bildar två tecken, ett *kort* och ett *långt*.

![Morsekod](morse.png)

Telegrafen förändrade vår världsbild. Plötsligt gick det att stå i direktkontakt med andra världsdelar. Vi fick en ny nyhetsförmedling och fler tidningar. Samtidigt kunde man för första gången känna av informationsstressen. Vi fick veta mängder av information som egentligen inte berörde oss.

## Tekniska system

Ett *system* är en helhet bestående av separata men samverkande delar. Under 1800-talet byggs stora tekniska system, t.ex. järnvägen. Järnvägen är centralt synkroniserad, men för att undvika krockar behövde alla samma tid. Med hjälp av telegrafen skickade man ut den gemensamma tiden över hela järnvägsnätet. Behovet av ett synkroniserat system tog man bort lokala tider och började använda tidszoner istället.

Bättre klockor spelade roll i andra tekniska system. Sjöfarten behövde exakta kronometrar för att veta var fartygen var i öst-västlig rikting. Den första ordentliga klockan för fartyg konstruerades först 1735.

Klockor är också det som bestämmer takten i en dator. Nästan alla tekniska system är idag helt beroende av klockor.

## Telefonen

Utveckling av telegrafen möjliggjorde till slut telefonen. Nu var man inte längre begränsad av morsekoden utan kunde säga vad man ville direkt. Svensken [Lars Magnus Ericsson](https://sv.wikipedia.org/wiki/Lars_Magnus_Ericsson) tillverka en egen telefon. Han grundade då tidigt [*Telefonaktiebolaget LM Ericsson*](https://sv.wikipedia.org/wiki/Ericsson). Eftersom telefonen inte krävde någon operatör var det äntligen möjligt att kunna ha maskinerna i hemmen, något som hade varit helt omöjligt med telegrafen.

I början kopplade *telefonister* upp samtalen, men i början av 1900-talet konstruerade man en [fingerskiva](https://en.wikipedia.org/wiki/Rotary_dial) som möjliggjorde mekaniska växlar.

## Radiotekniken

Hur ska man kunna kommunicera med båtar? Vidare forskning om magnetismen gjorde att man upptäckte radiovågorna. Detta gjordes av [James Clark Maxwell](https://en.wikipedia.org/wiki/James_Clerk_Maxwell) (1831-1879), som kom fram till att elektromagnetism och ljus är samma fenomen. Denna forskning beskrevs i exakta ekvationer som benämns [*Maxwells ekvationer*](https://en.wikipedia.org/wiki/Maxwell's_equations).

[Heinrich Hertz](https://en.wikipedia.org/wiki/Heinrich_Hertz) (1857-1894) arbetade med dessa ekvationer och lyckades sända och ta emot elektromagnetiska vågor. Han fick ocksä ge namn för enheten [*Hertz*](https://en.wikipedia.org/wiki/Hertz). [Guglielmo Marconi](https://en.wikipedia.org/wiki/Guglielmo_Marconi) (1874-1937) kommersialiserar radiotekniken och börjar sälja sändare och mottagare. 1897 grundar han bolaget [Marconi](https://en.wikipedia.org/wiki/Marconi_Company) som sedan 2006 är uppköpt av Ericsson.

1906 uppfinner man [*kristallmottagaren*](https://en.wikipedia.org/wiki/Crystal_radio), som möjliggör mycket billiga radioapparater. Amatörer börjar sända egna program. Efter några årtionden börjar staten att genomföra egna sändningar. I Sverige startar *Sverige Radiotjänst* sina sändningar 1924. I takt med att televisionen utvecklas sker samma utveckling kring rörliga bilder. Den första stora internationella sändningen var OS i Berlin 1936.

## 1800-talets teknikutveckling

Tekniken under 1800-talet utvecklades på ett väldigt ovetenskapligt sätt. Ofta tänker man att vetandet kommer före görandet, vetenskapen före tekniken. Snarare hittade man saker som fungerade och exploaterade dessa fenomen utan att vara säker på hur de fungerade. Först långt senare började man förstå hur allt hängde ihop.

Man kan säga att det finns två kunskapstraditioner:

* Den praktiska hantverkstraditionen
* Den skriftlärda, teoretiska

## Ingenjören

Ingenjörer löser problem genom att bygga. Tidigt fanns militära konstruktörer som benämndes ingenjörer. I början av 1700-talet börjar man i Tyskland utbilda civila ingenjörer. Under 1800-talet växer en modern ingenjörskår fram. Med tiden börjar den tekniska utbildningen vara jämbördig den vetenskapliga.

Efter ett tag kommer det två ingenjörsideal:

* Den klassiska ingenjören
    * Ska enbart fokusera på de tekniska problemen och lösa dessa med hjälp av matematik och vetenskapligt kunnande.
* Den moderna ingenjören
    * Ska kunna det mesta och är i slutändan den drivande kraften i företaget. Ofta startas och organiseras företag av ingenjörer.
