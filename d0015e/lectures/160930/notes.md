# Föreläsning 2016-09-30

## Haltproblemet

Matematikern David Hilbert frågade sig 1920,

> Är varje påstående som kan formuleras matematiskt antingen sant eller falskt?

Kurt Gödel visade att det inte var så. Det gick till exempel att formulera påståendet,

> Detta påstående är falskt.

Ett *påstående* är en satstyp med vilkens hjälp satsens användare hävdar att ett givet sakförhållande råder eller inte råder. Ett påstående är en sats som antingen är sanna eller falska, annars är det en paradox.

Alan Turing funderar istället på om en beräkning kommer att *terminera* eller *inte terminerar*. Han kom fram till att,

> För stegvisa beräkningar utförda av en maskin kan man inte alltid säga om de terminerar eller ej.


