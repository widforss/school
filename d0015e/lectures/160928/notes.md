# Föreläsning 2016-09-28

## Presentation

Målet med en presentation är att åhörarna ska uppfatta budskapet och tro på det. Presentationer utgår från hur vi människor fungerar. Genom att vara personligt närvarande fångar man åhörarnas uppmärksamhet på ett helt annat sätt än om man försöker sprida sitt budskap i text.

Presentationer möjliggör också dialog och är mer effektiv än andra kommunikationssätt. En bra presentation *engagerar, informerar* och *interagerar*. Det handlar inte om att läsa upp direkt från manus. Budskapet ska paketeras på ett sätt som passar åhörarna. Man måste ha rätt *innehåll, språk* och *form*. En presentation måste ha en bra röd tråd.

Man kan presentera på olika skalor. Presentationen man håller måste då anpassas efter antalet åhörarna.

### Inledningen

Under de första 15 sekunderna skapas kontakt mellan presentatör och åhörarna. Om man misslyckas med detta får åhörarna en förutfattad åsikt som är väldigt svår att radera. För att undvika rampfeber är det viktigt att förbereda allt noga.

* Vad ska visas
* Vad ska sägas
* Vem står var
* Hur är belysningen

### Retorisk argumentering

Du har tre argument, ett starkt, ett svagt och ett som är sisådär. Använd de då i följande ordning, det som är sisådär, det svaga och sist, det starka. De två första stödjer det sista, som människor kommer ihåg.

Folk i allmänhet är bara fokuserade i början och slutet av en föreläsning. Det kan man åtgärda genom att ha olika *"väckarklockor"*.

* Ställ ett exempel
* Använd en bild
* Ställ en fråga
* Provocera
* Skämta
* Bikupor

### Löpsedelteknik

Lägg upp din presentation som en löpsedel

* Rubrik
* Översikt
* Detaljer
* Djup
* Sammanfattning

![Artikel](artikel.png)

### Förberedelser

Ta reda på hur lokalen är inredd och dukad

* Var ska du vara i lokalen?
* Var sitter åhörarna?
* Uppskatta avstånd till sista raden, och till första.
* Vad finns det för hjälpmedel?
* Håll koll på utrustningen
* Utforma presentationen så att den passar lokalen

Anpassa presentaionen efter åhörarna

* Vad kan de?
* Vad har de för *kulturella, religiösa* och *ideologiska* skillnader mot presentatören.
* Förbered dig på frågor som åhörarna kan tänkas ställa.

Utgå inte från dig själv, utgå från åhörarna.

### Bildspelet

Använd ett format som kan visas. **Gör sidorna minimalistiska och enkla.** Fundera alltid på vad budskapet är och hur bildspelet stödjer detta.

Se Steve Jobs.

Granska textens läsbarhet. Använd sidregeln, max sex rader med max sex ord. En eller max två illustrationer per sida. Numrera sidor, bilder, foton med mera. Inkludera kontaktinformation i sidfoten.

Använd få färger på presentationen, helst ljus text på mörk bakgrund.

### Du själv

Klä dig så att du passar in. Alltid *hel och ren*. Ibland uppklädd, dock sällan på universitetet. Ät inte vitlök dagen innan.

Öva flitigt på presentationen. Tänk på vad du gör med armar och ben.

Ha också en low-tech plan B redo. Testa gärna all teknik i förväg. Ta reda på hur du ska starta. Blir du presenterad eller startar du själv?

### Talet

Prata fritt och huvudsakligen vänd mot åhörarna. Håll lagom tempo. Ta kommandot, var tydlig och led åhörarna. Överlåt inte till någon annan att dra slutsatser.

### Frågor

Bemöt kritik med att bejaka den. Skräm inte åhörarna och bjud in till frågor.

Upprepa ställda frågor så att alla åhörare hör dem innan du svarar.

### Kontakt via syn

Låt blicken svepa över åhörarna medan du pratar. Fäst blicken in i åhörarskaran då och då, men inte för länge.

### Kroppsspråk

Stå ledigt och still framför åhörarna mesta tiden. Skym inte bildspelet. Gå omkring måttligt mycket. 

Gestakulera lagom mycket. Det ska alltid vara genomtänkt och kontrollerat. Peka på sådant du vill att åhörarna ska se. Variera dig, men var dig själv.
