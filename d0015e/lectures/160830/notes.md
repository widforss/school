# Föreläsning 2016-08-30

PDF:
* [Från idé till prototyp med hjälp av moderna verktyg - LTU 160830 - Prof. Peter Parnes](./Från idé till prototyp med hjälp av moderna verktyg - LTU 160830 - Prof. Peter Parnes.pdf)
* [Universitet, lärare och sånt](l1univ.pdf)

## Hur funkar det på universitet?

Mycket på universitet är informellt och kreativt, men det finns vissa regler man måste hålla sig till.

Registrera er på [kursforumet](https://groups.google.com/forum/#!forum/d0015e-ht16-forum). Det är viktigt för individen och gruppen att alla är förberedda. Imorgon är det prov i A1016. Grupperna kommer att anslås vid dörren. På [kurshemsidan](https://sites.google.com/site/d0015eh15) står info om provet.

### LTU:s struktur

LTU är en statlig myndighet som lyder under Utbildningsdepartementet.

Det finns en universitetsstyrelse. Därunder rektor. Under rektorn finns verksamhetsstöd samt fakultetsnämnder. Nämnderna styr över forskning och utbildning.

Det finns också fakulteter som har den faktiska verksamheten. Fakulteten har en prefekt med avdelningar. Datavetenskap är en avdelning som själv innehåller flera ämnen. Dessa ämnen är indelningar av forskarna. Varje avdelning har också olika utbildningar.

Universitet regleras av Högskolelagen. Den är beslutad av riksdagen. Regeringen kan ge egna beslut. Dessa sammanfattas i Högskoleförordningen. Varje universitet har egna regler. Dessa finns hos UHR:s författningssamling.

Universitet ska bedriva forskning. Det handlar om att utöka det samlade vetandet. Detta oavsett rådande politik och ideologi.

* En forskare får fritt välja problem
* Metoder får fritt utvecklas
* Resultat får fritt publiceras

Universiteten ska anställa professorer och lektorer för undervisning och forskning.

### Disciplinära åtgärder

* Man får inte vilseleda (fuska)
* Man får inte störa verksamhet
* Man får inte trakassera andra

Störning ger varning eller avstängning. Den vanligaste anledningen till avstängning är fusk.

Dessutom gäller Sveriges rikes lag.

_Har_ni_problem_med_deadline,_prata_med_läraren._

## Från idé till prototyp med hjälp av moderna verktyg

Föreläsare: [Peter Parnes](mailto:peter@parnes.com)

### Idag

* Vad kommer det för teknik i framtiden
* Hur funkar det med säkerheten
* Teknik och jämställdhet

### Varför digitalisering?

Tidigare jobbade bara vissa grupper med datorer. Nu är datorer överallt. Det innebär att samhället förändras. Max har ordnat snabbkassor. De trodde att 10 % skulle använda dem, i realiteten är 75 %. Det innebär att det blir färre kassörer. Framöver kanske det är en maskin som steker hamburgaren också.

Förutom att det försvinner jobb så möjliggör tekniken även nya jobb.

Moores lag gör att teknik blir billigare och billigare. Den håller dock på att mattas av. Idag utvecklas CPU inte jättemycket, däremot händer det väldigt mycket inom GPU. De är idag extremt kraftfulla. En GPU har större kapacitet än 70-talets superdatorer. Detsamma gäller telefoner och andra mobila enheter.

### AI

Idag har man bytt taktik gällande automation (självkörande bilar). Tidigare ville man programmera in alla beteenden  i bilen manuellt. Nu vill man låta bilen lära sig själv genom att köra runt i trafiken. Lärandesättet man använder kallas *deep learning*. Datorer ska lära sig precis som ett barn gör, genom att uppleva. 

Självkörande bilar är väldigt intressant för lastbilstillverkare. Scania utvecklar idag lastbilståg, där många lastbilar delar på en chaufför. Vad händer då med alla lastbilschaufförer? De blir arbetslösa.
Programmen som lär sig så här är uppbyggda lite som våra hjärnor. De kallas neuronnätverk. En massa små delar lär sig om varsin liten del av problemet. Tillsammans blir alla delar oerhört kraftfulla. 

Ett neuronnät kan till exempel titta på en bild och känna igen objekt i bilden. Den kan också lära sig att måla enligt en konstnärs stil och applicera den på andra bilder. Maskinlärande kan också användas för viktminskning. Ariel Faigon loggade sina matvanor m.m. Efter några månader kunde ett neuronnätverk presentera för honom vad som gjorde att han gick upp i vikt.

Grace Hopper uppfann kompilatorn. Programmering var tidigt ett kvinnoyrke. En programmerare är lat. Man vill aldrig skriva redundant kod osv. Hopper var också lat. Hon ville inte skriva maskinkod, därav kompilatorn. Den hette Flow-Matic

Runt 2040 kommer vi att få en dator som har uppnått singulariteten. Det är en dator som är lika intelligent som en människa.

#### Juridik

AI kan också användas inom juridiken. Det finns idag nättjänster för att underlätta överklagande av parkeringsböter. Juristassistenter kan idag ofta ersättas av intelligenta system för att scanna och hitta relevanta rättsfall. Det används i USA idag ibland ett AI som hjälper en domare att fatta rätt beslut.

### Alltid uppkopplad

Vi har alltid internet med oss. Globalt är det dock bara 44 % av världens befolkning som har daglig tillgång till Internet. Det finns olika projekt för att ge internetuppkoppling i glesbefolkade områden. Det handlar om soldrivna flygplan och ballonger som ger täckning under sig. Dessa ska främst ersätta dragen fiber till basstationer.

Hur ser det ut 2045? Kanske har vi chip inom oss som ger hjärnan internetuppkoppling.

#### Moderna krig.

Dessa handlar främst om att slå ut infrastruktur. Detta är lätt med det nya systemet av Just in time. För några år sedan stal några personer kablar vid järnvägen. De klippte då av alla nät förutom Sunet. Dagens teknik har bristfällig redundans och säkerhet. Många kraftverk är enkla att slå ut via internet.

### Nya användargränssnitt

Vi har idag så kallade wearables. Vi _kan_ idag bära klockor och glasögon som är internetuppkopplade. Dessa kan användas för såväl allvar som lek. Det finns också nya inputs, till exempel pennor som man kan rita i ett tredimensionellt rum med. Det handlar alltså inte längre enbart om konsumtion utan VR börjar närma sig ordentlig interaktion.

Det jobbas på linser som displayer. De är väldigt lågupplösta idag men kommer att bli bättre.

### Prata med datorer

Kan man ställa en fråga till en dator och få ett vettigt svar? Siri funkar idag helt ok, detta på grund av att man har jobbat mycket mer med maskinlärande. Maskinlärande byggs också in i dagens klienter, t.ex. iOS 10.

### Nya utbildningar

Många universitet levererar idag gratis utbildningar. De är ganska effektiva för motiverade studenter och godtas av vissa arbetsgivare. Hur kommer utbildningar förändras i framtiden?

### Hur ser framtidens arbetsmarknad ut?

20-50 % av dagens arbeten kommer att ersättas av datorer. Istället kommer nya arbetsgrupper att uppstå. Mellanchefer kommer att försvinna.

Morgondagens arbetare kommer att arbeta

* självständigt
* från idé till produkt
* i små team
* med stor egen påverkan
* snabbt, med kort horisont
* internationellt
* på en global marknad

### Digitala medborgare

För att kunna vara en informerad medborgare måste man förstå datatekniken. Man måste förstå vad för information som man medvetet eller omedvetet delar med andra aktörer.

### Skaparkultur

Hur kan vi använda tekniken i vår kreativitet?

* Spelinspirerat godis
* elektroniskt luciatåg
* elektroniska smycken
* 3d-utskrifter
* osv osv

### Jämställdhet

Parnes driver #include, ett arbete för att behålla de få kvinnor vi har inom området.
